const StyleDictionary = require('style-dictionary')

// https://amzn.github.io/style-dictionary/#/
const StyleDictionaryExtended = StyleDictionary.extend(require('./config.json'))
StyleDictionaryExtended.buildAllPlatforms()
