OurVerses Design
===============
This project is for keeping track of the global styles inside the Figma design tool, exporting them as universal design tokens, and then transforming them to the stylesheet files for any of the client application platforms that need them.

## Prerequisites
* Have node and npm installed on your system

## Install
* npm install

## Usage
* Open the design file in [Figma](https://www.figma.com/downloads/): https://www.figma.com/file/Lq0dDQBR4S6vCsOhmlRXT1/Material-2-Design-System?node-id=4%3A0
* Find `Plugins > Design Tokens > Export Design Tokens File`.
* Save file as 'our.tokens.json' inside this local project (overwrite existing).
* From terminal, run `npm run build`. This will look for that .json file, compile it with [style-dictionary](https://github.com/amzn/style-dictionary), and create output files in `/build` directory.

## Commit and push
* Commit and push your changes (including the output in tbe `/build` directory) up to the repo.
