
//
// style_dictionary.dart
//

// Do not edit directly
// Generated on Mon, 17 Jan 2022 17:06:35 GMT



import 'dart:ui';

class StyleDictionary {
  StyleDictionary._();

    static const colorM3ReadOnlyDarkBackgroundOpacity008 = Color(0x141C1B1F);
    static const colorM3ReadOnlyDarkBackgroundOpacity012 = Color(0x1F1C1B1F);
    static const colorM3ReadOnlyDarkBackgroundOpacity016 = Color(0x291C1B1F);
    static const colorM3ReadOnlyDarkBlack = Color(0xFF000000);
    static const colorM3ReadOnlyDarkErrorContainerOpacity008 = Color(0x148C1D18);
    static const colorM3ReadOnlyDarkErrorContainerOpacity012 = Color(0x1F8C1D18);
    static const colorM3ReadOnlyDarkErrorContainerOpacity016 = Color(0x298C1D18);
    static const colorM3ReadOnlyDarkErrorOpacity008 = Color(0x14F2B8B5);
    static const colorM3ReadOnlyDarkErrorOpacity012 = Color(0x1FF2B8B5);
    static const colorM3ReadOnlyDarkErrorOpacity016 = Color(0x29F2B8B5);
    static const colorM3ReadOnlyDarkInverseOnSurfaceOpacity008 = Color(0x141C1B1F);
    static const colorM3ReadOnlyDarkInverseOnSurfaceOpacity012 = Color(0x1F1C1B1F);
    static const colorM3ReadOnlyDarkInverseOnSurfaceOpacity016 = Color(0x291C1B1F);
    static const colorM3ReadOnlyDarkInverseSurfaceOpacity008 = Color(0x14E6E1E5);
    static const colorM3ReadOnlyDarkInverseSurfaceOpacity012 = Color(0x1FE6E1E5);
    static const colorM3ReadOnlyDarkInverseSurfaceOpacity016 = Color(0x29E6E1E5);
    static const colorM3ReadOnlyDarkOnBackgroundOpacity008 = Color(0x14E6E1E5);
    static const colorM3ReadOnlyDarkOnBackgroundOpacity012 = Color(0x1FE6E1E5);
    static const colorM3ReadOnlyDarkOnBackgroundOpacity016 = Color(0x29E6E1E5);
    static const colorM3ReadOnlyDarkOnErrorContainerOpacity008 = Color(0x14F9DEDC);
    static const colorM3ReadOnlyDarkOnErrorContainerOpacity012 = Color(0x1FF9DEDC);
    static const colorM3ReadOnlyDarkOnErrorContainerOpacity016 = Color(0x29F9DEDC);
    static const colorM3ReadOnlyDarkOnErrorOpacity008 = Color(0x14601410);
    static const colorM3ReadOnlyDarkOnErrorOpacity012 = Color(0x1F601410);
    static const colorM3ReadOnlyDarkOnErrorOpacity016 = Color(0x29601410);
    static const colorM3ReadOnlyDarkOnPrimaryContainerOpacity008 = Color(0x14EADDFF);
    static const colorM3ReadOnlyDarkOnPrimaryContainerOpacity012 = Color(0x1FEADDFF);
    static const colorM3ReadOnlyDarkOnPrimaryContainerOpacity016 = Color(0x29EADDFF);
    static const colorM3ReadOnlyDarkOnPrimaryOpacity008 = Color(0x14381E72);
    static const colorM3ReadOnlyDarkOnPrimaryOpacity012 = Color(0x1F381E72);
    static const colorM3ReadOnlyDarkOnPrimaryOpacity016 = Color(0x29381E72);
    static const colorM3ReadOnlyDarkOnSecondaryContainerOpacity008 = Color(0x14E8DEF8);
    static const colorM3ReadOnlyDarkOnSecondaryContainerOpacity012 = Color(0x1FE8DEF8);
    static const colorM3ReadOnlyDarkOnSecondaryContainerOpacity016 = Color(0x29E8DEF8);
    static const colorM3ReadOnlyDarkOnSecondaryOpacity008 = Color(0x14332D41);
    static const colorM3ReadOnlyDarkOnSecondaryOpacity012 = Color(0x1F332D41);
    static const colorM3ReadOnlyDarkOnSecondaryOpacity016 = Color(0x29332D41);
    static const colorM3ReadOnlyDarkOnSurfaceOpacity008 = Color(0x14E6E1E5);
    static const colorM3ReadOnlyDarkOnSurfaceOpacity012 = Color(0x1FE6E1E5);
    static const colorM3ReadOnlyDarkOnSurfaceOpacity016 = Color(0x29E6E1E5);
    static const colorM3ReadOnlyDarkOnSurfaceVariantOpacity008 = Color(0x14CAC4D0);
    static const colorM3ReadOnlyDarkOnSurfaceVariantOpacity012 = Color(0x1FCAC4D0);
    static const colorM3ReadOnlyDarkOnSurfaceVariantOpacity016 = Color(0x29CAC4D0);
    static const colorM3ReadOnlyDarkOnTertiaryContainerOpacity008 = Color(0x14FFD8E4);
    static const colorM3ReadOnlyDarkOnTertiaryContainerOpacity012 = Color(0x1FFFD8E4);
    static const colorM3ReadOnlyDarkOnTertiaryContainerOpacity016 = Color(0x29FFD8E4);
    static const colorM3ReadOnlyDarkOnTertiaryOpacity008 = Color(0x14492532);
    static const colorM3ReadOnlyDarkOnTertiaryOpacity012 = Color(0x1F492532);
    static const colorM3ReadOnlyDarkOnTertiaryOpacity016 = Color(0x29492532);
    static const colorM3ReadOnlyDarkOutlineOpacity008 = Color(0x14938F99);
    static const colorM3ReadOnlyDarkOutlineOpacity012 = Color(0x1F938F99);
    static const colorM3ReadOnlyDarkOutlineOpacity016 = Color(0x29938F99);
    static const colorM3ReadOnlyDarkPrimaryContainerOpacity008 = Color(0x144F378B);
    static const colorM3ReadOnlyDarkPrimaryContainerOpacity012 = Color(0x1F4F378B);
    static const colorM3ReadOnlyDarkPrimaryContainerOpacity016 = Color(0x294F378B);
    static const colorM3ReadOnlyDarkPrimaryOpacity008 = Color(0x14D0BCFF);
    static const colorM3ReadOnlyDarkPrimaryOpacity012 = Color(0x1FD0BCFF);
    static const colorM3ReadOnlyDarkPrimaryOpacity016 = Color(0x29D0BCFF);
    static const colorM3ReadOnlyDarkSecondaryContainerOpacity008 = Color(0x144A4458);
    static const colorM3ReadOnlyDarkSecondaryContainerOpacity012 = Color(0x1F4A4458);
    static const colorM3ReadOnlyDarkSecondaryContainerOpacity016 = Color(0x294A4458);
    static const colorM3ReadOnlyDarkSecondaryOpacity008 = Color(0x14CCC2DC);
    static const colorM3ReadOnlyDarkSecondaryOpacity012 = Color(0x1FCCC2DC);
    static const colorM3ReadOnlyDarkSecondaryOpacity016 = Color(0x29CCC2DC);
    static const colorM3ReadOnlyDarkSurface10 = Color(0xFF1C1B1F);
    static const colorM3ReadOnlyDarkSurface11 = Color(0x0DD0BCFF);
    static const colorM3ReadOnlyDarkSurface20 = Color(0xFF1C1B1F);
    static const colorM3ReadOnlyDarkSurface21 = Color(0x14D0BCFF);
    static const colorM3ReadOnlyDarkSurface30 = Color(0xFF1C1B1F);
    static const colorM3ReadOnlyDarkSurface31 = Color(0x1CD0BCFF);
    static const colorM3ReadOnlyDarkSurface40 = Color(0xFF1C1B1F);
    static const colorM3ReadOnlyDarkSurface41 = Color(0x1FD0BCFF);
    static const colorM3ReadOnlyDarkSurface50 = Color(0xFF1C1B1F);
    static const colorM3ReadOnlyDarkSurface51 = Color(0x24D0BCFF);
    static const colorM3ReadOnlyDarkSurfaceOpacity008 = Color(0x141C1B1F);
    static const colorM3ReadOnlyDarkSurfaceOpacity012 = Color(0x1F1C1B1F);
    static const colorM3ReadOnlyDarkSurfaceOpacity016 = Color(0x291C1B1F);
    static const colorM3ReadOnlyDarkSurfaceVariantOpacity008 = Color(0x1449454F);
    static const colorM3ReadOnlyDarkSurfaceVariantOpacity012 = Color(0x1F49454F);
    static const colorM3ReadOnlyDarkSurfaceVariantOpacity016 = Color(0x2949454F);
    static const colorM3ReadOnlyDarkTertiaryContainerOpacity008 = Color(0x14633B48);
    static const colorM3ReadOnlyDarkTertiaryContainerOpacity012 = Color(0x1F633B48);
    static const colorM3ReadOnlyDarkTertiaryContainerOpacity016 = Color(0x29633B48);
    static const colorM3ReadOnlyDarkTertiaryOpacity008 = Color(0x14EFB8C8);
    static const colorM3ReadOnlyDarkTertiaryOpacity012 = Color(0x1FEFB8C8);
    static const colorM3ReadOnlyDarkTertiaryOpacity016 = Color(0x29EFB8C8);
    static const colorM3ReadOnlyDarkWhite = Color(0xFFFFFFFF);
    static const colorM3ReadOnlyLightBackgroundOpacity008 = Color(0x14FFFBFE);
    static const colorM3ReadOnlyLightBackgroundOpacity012 = Color(0x1FFFFBFE);
    static const colorM3ReadOnlyLightBackgroundOpacity016 = Color(0x29FFFBFE);
    static const colorM3ReadOnlyLightBlack = Color(0xFF000000);
    static const colorM3ReadOnlyLightErrorContainerOpacity008 = Color(0x14F9DEDC);
    static const colorM3ReadOnlyLightErrorContainerOpacity012 = Color(0x1FF9DEDC);
    static const colorM3ReadOnlyLightErrorContainerOpacity016 = Color(0x29F9DEDC);
    static const colorM3ReadOnlyLightErrorOpacity008 = Color(0x14B3261E);
    static const colorM3ReadOnlyLightErrorOpacity012 = Color(0x1FB3261E);
    static const colorM3ReadOnlyLightErrorOpacity016 = Color(0x29B3261E);
    static const colorM3ReadOnlyLightInverseOnSurfaceOpacity008 = Color(0x14F4EFF4);
    static const colorM3ReadOnlyLightInverseOnSurfaceOpacity012 = Color(0x1FF4EFF4);
    static const colorM3ReadOnlyLightInverseOnSurfaceOpacity016 = Color(0x29F4EFF4);
    static const colorM3ReadOnlyLightInverseSurfaceOpacity008 = Color(0x14313033);
    static const colorM3ReadOnlyLightInverseSurfaceOpacity012 = Color(0x1F313033);
    static const colorM3ReadOnlyLightInverseSurfaceOpacity016 = Color(0x29313033);
    static const colorM3ReadOnlyLightOnBackgroundOpacity008 = Color(0x141C1B1F);
    static const colorM3ReadOnlyLightOnBackgroundOpacity012 = Color(0x1F1C1B1F);
    static const colorM3ReadOnlyLightOnBackgroundOpacity016 = Color(0x291C1B1F);
    static const colorM3ReadOnlyLightOnErrorContainerOpacity008 = Color(0x14410E0B);
    static const colorM3ReadOnlyLightOnErrorContainerOpacity012 = Color(0x1F410E0B);
    static const colorM3ReadOnlyLightOnErrorContainerOpacity016 = Color(0x29410E0B);
    static const colorM3ReadOnlyLightOnErrorOpacity008 = Color(0x14FFFFFF);
    static const colorM3ReadOnlyLightOnErrorOpacity012 = Color(0x1FFFFFFF);
    static const colorM3ReadOnlyLightOnErrorOpacity016 = Color(0x29FFFFFF);
    static const colorM3ReadOnlyLightOnPrimaryContainerOpacity008 = Color(0x1421005D);
    static const colorM3ReadOnlyLightOnPrimaryContainerOpacity012 = Color(0x1F21005D);
    static const colorM3ReadOnlyLightOnPrimaryContainerOpacity016 = Color(0x2921005D);
    static const colorM3ReadOnlyLightOnPrimaryOpacity008 = Color(0x14FFFFFF);
    static const colorM3ReadOnlyLightOnPrimaryOpacity012 = Color(0x1FFFFFFF);
    static const colorM3ReadOnlyLightOnPrimaryOpacity016 = Color(0x29FFFFFF);
    static const colorM3ReadOnlyLightOnSecondaryContainerOpacity008 = Color(0x141D192B);
    static const colorM3ReadOnlyLightOnSecondaryContainerOpacity012 = Color(0x1F1D192B);
    static const colorM3ReadOnlyLightOnSecondaryContainerOpacity016 = Color(0x291D192B);
    static const colorM3ReadOnlyLightOnSecondaryOpacity008 = Color(0x14FFFFFF);
    static const colorM3ReadOnlyLightOnSecondaryOpacity012 = Color(0x1FFFFFFF);
    static const colorM3ReadOnlyLightOnSecondaryOpacity016 = Color(0x29FFFFFF);
    static const colorM3ReadOnlyLightOnSurfaceOpacity008 = Color(0x141C1B1F);
    static const colorM3ReadOnlyLightOnSurfaceOpacity012 = Color(0x1F1C1B1F);
    static const colorM3ReadOnlyLightOnSurfaceOpacity016 = Color(0x291C1B1F);
    static const colorM3ReadOnlyLightOnSurfaceVariantOpacity008 = Color(0x1449454F);
    static const colorM3ReadOnlyLightOnSurfaceVariantOpacity012 = Color(0x1F49454F);
    static const colorM3ReadOnlyLightOnSurfaceVariantOpacity016 = Color(0x2949454F);
    static const colorM3ReadOnlyLightOnTertiaryContainerOpacity008 = Color(0x1431111D);
    static const colorM3ReadOnlyLightOnTertiaryContainerOpacity012 = Color(0x1F31111D);
    static const colorM3ReadOnlyLightOnTertiaryContainerOpacity016 = Color(0x2931111D);
    static const colorM3ReadOnlyLightOnTertiaryOpacity008 = Color(0x14FFFFFF);
    static const colorM3ReadOnlyLightOnTertiaryOpacity012 = Color(0x1FFFFFFF);
    static const colorM3ReadOnlyLightOnTertiaryOpacity016 = Color(0x29FFFFFF);
    static const colorM3ReadOnlyLightOutlineOpacity008 = Color(0x1479747E);
    static const colorM3ReadOnlyLightOutlineOpacity012 = Color(0x1F79747E);
    static const colorM3ReadOnlyLightOutlineOpacity016 = Color(0x2979747E);
    static const colorM3ReadOnlyLightPrimaryContainerOpacity008 = Color(0x14EADDFF);
    static const colorM3ReadOnlyLightPrimaryContainerOpacity012 = Color(0x1FEADDFF);
    static const colorM3ReadOnlyLightPrimaryContainerOpacity016 = Color(0x29EADDFF);
    static const colorM3ReadOnlyLightPrimaryOpacity008 = Color(0x146750A4);
    static const colorM3ReadOnlyLightPrimaryOpacity012 = Color(0x1F6750A4);
    static const colorM3ReadOnlyLightPrimaryOpacity016 = Color(0x296750A4);
    static const colorM3ReadOnlyLightSecondaryContainerOpacity008 = Color(0x14E8DEF8);
    static const colorM3ReadOnlyLightSecondaryContainerOpacity012 = Color(0x1FE8DEF8);
    static const colorM3ReadOnlyLightSecondaryContainerOpacity016 = Color(0x29E8DEF8);
    static const colorM3ReadOnlyLightSecondaryOpacity008 = Color(0x14625B71);
    static const colorM3ReadOnlyLightSecondaryOpacity012 = Color(0x1F625B71);
    static const colorM3ReadOnlyLightSecondaryOpacity016 = Color(0x29625B71);
    static const colorM3ReadOnlyLightSurface10 = Color(0xFFFFFBFE);
    static const colorM3ReadOnlyLightSurface11 = Color(0x0D6750A4);
    static const colorM3ReadOnlyLightSurface20 = Color(0xFFFFFBFE);
    static const colorM3ReadOnlyLightSurface21 = Color(0x146750A4);
    static const colorM3ReadOnlyLightSurface30 = Color(0xFFFFFBFE);
    static const colorM3ReadOnlyLightSurface31 = Color(0x1C6750A4);
    static const colorM3ReadOnlyLightSurface40 = Color(0xFFFFFBFE);
    static const colorM3ReadOnlyLightSurface41 = Color(0x1F6750A4);
    static const colorM3ReadOnlyLightSurface50 = Color(0xFFFFFBFE);
    static const colorM3ReadOnlyLightSurface51 = Color(0x246750A4);
    static const colorM3ReadOnlyLightSurfaceOpacity008 = Color(0x14FFFBFE);
    static const colorM3ReadOnlyLightSurfaceOpacity012 = Color(0x1FFFFBFE);
    static const colorM3ReadOnlyLightSurfaceOpacity016 = Color(0x29FFFBFE);
    static const colorM3ReadOnlyLightSurfaceVariantOpacity008 = Color(0x14E7E0EC);
    static const colorM3ReadOnlyLightSurfaceVariantOpacity012 = Color(0x1FE7E0EC);
    static const colorM3ReadOnlyLightSurfaceVariantOpacity016 = Color(0x29E7E0EC);
    static const colorM3ReadOnlyLightTertiaryContainerOpacity008 = Color(0x14FFD8E4);
    static const colorM3ReadOnlyLightTertiaryContainerOpacity012 = Color(0x1FFFD8E4);
    static const colorM3ReadOnlyLightTertiaryContainerOpacity016 = Color(0x29FFD8E4);
    static const colorM3ReadOnlyLightTertiaryOpacity008 = Color(0x147D5260);
    static const colorM3ReadOnlyLightTertiaryOpacity012 = Color(0x1F7D5260);
    static const colorM3ReadOnlyLightTertiaryOpacity016 = Color(0x297D5260);
    static const colorM3ReadOnlyLightWhite = Color(0xFFFFFFFF);
    static const colorM3RefErrorError0 = Color(0xFF000000);
    static const colorM3RefErrorError10 = Color(0xFF410E0B);
    static const colorM3RefErrorError100 = Color(0xFFFFFFFF);
    static const colorM3RefErrorError20 = Color(0xFF601410);
    static const colorM3RefErrorError30 = Color(0xFF8C1D18);
    static const colorM3RefErrorError40 = Color(0xFFB3261E);
    static const colorM3RefErrorError50 = Color(0xFFDC362E);
    static const colorM3RefErrorError60 = Color(0xFFE46962);
    static const colorM3RefErrorError70 = Color(0xFFEC928E);
    static const colorM3RefErrorError80 = Color(0xFFF2B8B5);
    static const colorM3RefErrorError90 = Color(0xFFF9DEDC);
    static const colorM3RefErrorError95 = Color(0xFFFCEEEE);
    static const colorM3RefErrorError99 = Color(0xFFFFFBF9);
    static const colorM3RefNeutralNeutral0 = Color(0xFF000000);
    static const colorM3RefNeutralNeutral10 = Color(0xFF1C1B1F);
    static const colorM3RefNeutralNeutral100 = Color(0xFFFFFFFF);
    static const colorM3RefNeutralNeutral20 = Color(0xFF313033);
    static const colorM3RefNeutralNeutral30 = Color(0xFF484649);
    static const colorM3RefNeutralNeutral40 = Color(0xFF605D62);
    static const colorM3RefNeutralNeutral50 = Color(0xFF787579);
    static const colorM3RefNeutralNeutral60 = Color(0xFF939094);
    static const colorM3RefNeutralNeutral70 = Color(0xFFAEAAAE);
    static const colorM3RefNeutralNeutral80 = Color(0xFFC9C5CA);
    static const colorM3RefNeutralNeutral90 = Color(0xFFE6E1E5);
    static const colorM3RefNeutralNeutral95 = Color(0xFFF4EFF4);
    static const colorM3RefNeutralNeutral99 = Color(0xFFFFFBFE);
    static const colorM3RefNeutralVariantNeutralVariant0 = Color(0xFF000000);
    static const colorM3RefNeutralVariantNeutralVariant10 = Color(0xFF1D1A22);
    static const colorM3RefNeutralVariantNeutralVariant100 = Color(0xFFFFFFFF);
    static const colorM3RefNeutralVariantNeutralVariant20 = Color(0xFF322F37);
    static const colorM3RefNeutralVariantNeutralVariant30 = Color(0xFF49454F);
    static const colorM3RefNeutralVariantNeutralVariant40 = Color(0xFF605D66);
    static const colorM3RefNeutralVariantNeutralVariant50 = Color(0xFF79747E);
    static const colorM3RefNeutralVariantNeutralVariant60 = Color(0xFF938F99);
    static const colorM3RefNeutralVariantNeutralVariant70 = Color(0xFFAEA9B4);
    static const colorM3RefNeutralVariantNeutralVariant80 = Color(0xFFCAC4D0);
    static const colorM3RefNeutralVariantNeutralVariant90 = Color(0xFFE7E0EC);
    static const colorM3RefNeutralVariantNeutralVariant95 = Color(0xFFF5EEFA);
    static const colorM3RefNeutralVariantNeutralVariant99 = Color(0xFFFFFBFE);
    static const colorM3RefPrimaryPrimary0 = Color(0xFF000000);
    static const colorM3RefPrimaryPrimary10 = Color(0xFF21005D);
    static const colorM3RefPrimaryPrimary100 = Color(0xFFFFFFFF);
    static const colorM3RefPrimaryPrimary20 = Color(0xFF381E72);
    static const colorM3RefPrimaryPrimary30 = Color(0xFF4F378B);
    static const colorM3RefPrimaryPrimary40 = Color(0xFF6750A4);
    static const colorM3RefPrimaryPrimary50 = Color(0xFF7F67BE);
    static const colorM3RefPrimaryPrimary60 = Color(0xFF9A82DB);
    static const colorM3RefPrimaryPrimary70 = Color(0xFFB69DF8);
    static const colorM3RefPrimaryPrimary80 = Color(0xFFD0BCFF);
    static const colorM3RefPrimaryPrimary90 = Color(0xFFEADDFF);
    static const colorM3RefPrimaryPrimary95 = Color(0xFFF6EDFF);
    static const colorM3RefPrimaryPrimary99 = Color(0xFFFFFBFE);
    static const colorM3RefSecondarySecondary0 = Color(0xFF000000);
    static const colorM3RefSecondarySecondary10 = Color(0xFF1D192B);
    static const colorM3RefSecondarySecondary100 = Color(0xFFFFFFFF);
    static const colorM3RefSecondarySecondary20 = Color(0xFF332D41);
    static const colorM3RefSecondarySecondary30 = Color(0xFF4A4458);
    static const colorM3RefSecondarySecondary40 = Color(0xFF625B71);
    static const colorM3RefSecondarySecondary50 = Color(0xFF7A7289);
    static const colorM3RefSecondarySecondary60 = Color(0xFF958DA5);
    static const colorM3RefSecondarySecondary70 = Color(0xFFB0A7C0);
    static const colorM3RefSecondarySecondary80 = Color(0xFFCCC2DC);
    static const colorM3RefSecondarySecondary90 = Color(0xFFE8DEF8);
    static const colorM3RefSecondarySecondary95 = Color(0xFFF6EDFF);
    static const colorM3RefSecondarySecondary99 = Color(0xFFFFFBFE);
    static const colorM3RefTertiaryTertiary0 = Color(0xFF000000);
    static const colorM3RefTertiaryTertiary10 = Color(0xFF31111D);
    static const colorM3RefTertiaryTertiary100 = Color(0xFFFFFFFF);
    static const colorM3RefTertiaryTertiary20 = Color(0xFF492532);
    static const colorM3RefTertiaryTertiary30 = Color(0xFF633B48);
    static const colorM3RefTertiaryTertiary40 = Color(0xFF7D5260);
    static const colorM3RefTertiaryTertiary50 = Color(0xFF986977);
    static const colorM3RefTertiaryTertiary60 = Color(0xFFB58392);
    static const colorM3RefTertiaryTertiary70 = Color(0xFFD29DAC);
    static const colorM3RefTertiaryTertiary80 = Color(0xFFEFB8C8);
    static const colorM3RefTertiaryTertiary90 = Color(0xFFFFD8E4);
    static const colorM3RefTertiaryTertiary95 = Color(0xFFFFECF1);
    static const colorM3RefTertiaryTertiary99 = Color(0xFFFFFBFA);
    static const colorM3SourceError = Color(0xFFB3261E);
    static const colorM3SourceNeutral = Color(0xFF605D62);
    static const colorM3SourceNeutralVariant = Color(0xFF605D66);
    static const colorM3SourcePrimary = Color(0xFF6750A4);
    static const colorM3SourceSecondary = Color(0xFF625B71);
    static const colorM3SourceSeed = Color(0xFF6750A4);
    static const colorM3SourceTertiary = Color(0xFF7D5260);
    static const colorM3SysDarkBackground = Color(0xFF1C1B1F); /* background : on-background

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkError = Color(0xFFF2B8B5); /* error : on-error

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkErrorContainer = Color(0xFF8C1D18); /* error-container : on-error-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkInverseOnSurface = Color(0xFF1C1B1F);
    static const colorM3SysDarkInverseSurface = Color(0xFFE6E1E5);
    static const colorM3SysDarkOnBackground = Color(0xFFE6E1E5);
    static const colorM3SysDarkOnError = Color(0xFF601410);
    static const colorM3SysDarkOnErrorContainer = Color(0xFFF9DEDC);
    static const colorM3SysDarkOnPrimary = Color(0xFF381E72);
    static const colorM3SysDarkOnPrimaryContainer = Color(0xFFEADDFF);
    static const colorM3SysDarkOnSecondary = Color(0xFF332D41);
    static const colorM3SysDarkOnSecondaryContainer = Color(0xFFE8DEF8);
    static const colorM3SysDarkOnSurface = Color(0xFFE6E1E5);
    static const colorM3SysDarkOnSurfaceVariant = Color(0xFFCAC4D0);
    static const colorM3SysDarkOnTertiary = Color(0xFF492532);
    static const colorM3SysDarkOnTertiaryContainer = Color(0xFFFFD8E4);
    static const colorM3SysDarkOutline = Color(0xFF938F99);
    static const colorM3SysDarkPrimary = Color(0xFFD0BCFF); /* primary : on-primary

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkPrimaryContainer = Color(0xFF4F378B); /* primary-container : on-primary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkSecondary = Color(0xFFCCC2DC); /* secondary : on-secondary

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkSecondaryContainer = Color(0xFF4A4458); /* secondary-container : on-secondary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkSurface = Color(0xFF1C1B1F); /* surface : on-surface

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkSurfaceVariant = Color(0xFF49454F); /* surface-variant : on-surface-variant

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkTertiary = Color(0xFFEFB8C8); /* tertiary : on-tertiary

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysDarkTertiaryContainer = Color(0xFF633B48); /* tertiary-container : on-tertiary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightBackground = Color(0xFFFFFBFE); /* background : on-background

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightError = Color(0xFFB3261E); /* error : on-error

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightErrorContainer = Color(0xFFF9DEDC); /* error-container : on-error-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightInverseOnSurface = Color(0xFFF4EFF4);
    static const colorM3SysLightInverseSurface = Color(0xFF313033);
    static const colorM3SysLightOnBackground = Color(0xFF1C1B1F);
    static const colorM3SysLightOnError = Color(0xFFFFFFFF);
    static const colorM3SysLightOnErrorContainer = Color(0xFF410E0B);
    static const colorM3SysLightOnPrimary = Color(0xFFFFFFFF);
    static const colorM3SysLightOnPrimaryContainer = Color(0xFF21005D);
    static const colorM3SysLightOnSecondary = Color(0xFFFFFFFF);
    static const colorM3SysLightOnSecondaryContainer = Color(0xFF1D192B);
    static const colorM3SysLightOnSurface = Color(0xFF1C1B1F);
    static const colorM3SysLightOnSurfaceVariant = Color(0xFF49454F);
    static const colorM3SysLightOnTertiary = Color(0xFFFFFFFF);
    static const colorM3SysLightOnTertiaryContainer = Color(0xFF31111D);
    static const colorM3SysLightOutline = Color(0xFF79747E);
    static const colorM3SysLightPrimary = Color(0xFF6750A4); /* primary : on-primary

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightPrimaryContainer = Color(0xFFEADDFF); /* primary-container : on-primary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightSecondary = Color(0xFF625B71); /* secondary : on-secondary

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightSecondaryContainer = Color(0xFFE8DEF8); /* secondary-container : on-secondary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightSurface = Color(0xFFFFFBFE); /* surface : on-surface

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightSurfaceVariant = Color(0xFFE7E0EC); /* surface-variant : on-surface-variant

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightTertiary = Color(0xFF7D5260); /* tertiary : on-tertiary

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const colorM3SysLightTertiaryContainer = Color(0xFFFFD8E4); /* tertiary-container : on-tertiary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const effectM3ElevationDark10Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationDark10OffsetX = 0;
    static const effectM3ElevationDark10OffsetY = 1;
    static const effectM3ElevationDark10Radius = 2;
    static const effectM3ElevationDark10Spread = 0;
    static const effectM3ElevationDark10Type = dropShadow;
    static const effectM3ElevationDark11Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationDark11OffsetX = 0;
    static const effectM3ElevationDark11OffsetY = 1;
    static const effectM3ElevationDark11Radius = 3;
    static const effectM3ElevationDark11Spread = 1;
    static const effectM3ElevationDark11Type = dropShadow;
    static const effectM3ElevationDark20Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationDark20OffsetX = 0;
    static const effectM3ElevationDark20OffsetY = 1;
    static const effectM3ElevationDark20Radius = 2;
    static const effectM3ElevationDark20Spread = 0;
    static const effectM3ElevationDark20Type = dropShadow;
    static const effectM3ElevationDark21Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationDark21OffsetX = 0;
    static const effectM3ElevationDark21OffsetY = 2;
    static const effectM3ElevationDark21Radius = 6;
    static const effectM3ElevationDark21Spread = 2;
    static const effectM3ElevationDark21Type = dropShadow;
    static const effectM3ElevationDark30Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationDark30OffsetX = 0;
    static const effectM3ElevationDark30OffsetY = 1;
    static const effectM3ElevationDark30Radius = 3;
    static const effectM3ElevationDark30Spread = 0;
    static const effectM3ElevationDark30Type = dropShadow;
    static const effectM3ElevationDark31Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationDark31OffsetX = 0;
    static const effectM3ElevationDark31OffsetY = 4;
    static const effectM3ElevationDark31Radius = 8;
    static const effectM3ElevationDark31Spread = 3;
    static const effectM3ElevationDark31Type = dropShadow;
    static const effectM3ElevationDark40Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationDark40OffsetX = 0;
    static const effectM3ElevationDark40OffsetY = 2;
    static const effectM3ElevationDark40Radius = 3;
    static const effectM3ElevationDark40Spread = 0;
    static const effectM3ElevationDark40Type = dropShadow;
    static const effectM3ElevationDark41Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationDark41OffsetX = 0;
    static const effectM3ElevationDark41OffsetY = 6;
    static const effectM3ElevationDark41Radius = 10;
    static const effectM3ElevationDark41Spread = 4;
    static const effectM3ElevationDark41Type = dropShadow;
    static const effectM3ElevationDark50Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationDark50OffsetX = 0;
    static const effectM3ElevationDark50OffsetY = 4;
    static const effectM3ElevationDark50Radius = 4;
    static const effectM3ElevationDark50Spread = 0;
    static const effectM3ElevationDark50Type = dropShadow;
    static const effectM3ElevationDark51Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationDark51OffsetX = 0;
    static const effectM3ElevationDark51OffsetY = 8;
    static const effectM3ElevationDark51Radius = 12;
    static const effectM3ElevationDark51Spread = 6;
    static const effectM3ElevationDark51Type = dropShadow;
    static const effectM3ElevationLight10Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationLight10OffsetX = 0;
    static const effectM3ElevationLight10OffsetY = 1;
    static const effectM3ElevationLight10Radius = 3;
    static const effectM3ElevationLight10Spread = 1;
    static const effectM3ElevationLight10Type = dropShadow;
    static const effectM3ElevationLight11Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationLight11OffsetX = 0;
    static const effectM3ElevationLight11OffsetY = 1;
    static const effectM3ElevationLight11Radius = 2;
    static const effectM3ElevationLight11Spread = 0;
    static const effectM3ElevationLight11Type = dropShadow;
    static const effectM3ElevationLight20Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationLight20OffsetX = 0;
    static const effectM3ElevationLight20OffsetY = 2;
    static const effectM3ElevationLight20Radius = 6;
    static const effectM3ElevationLight20Spread = 2;
    static const effectM3ElevationLight20Type = dropShadow;
    static const effectM3ElevationLight21Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationLight21OffsetX = 0;
    static const effectM3ElevationLight21OffsetY = 1;
    static const effectM3ElevationLight21Radius = 2;
    static const effectM3ElevationLight21Spread = 0;
    static const effectM3ElevationLight21Type = dropShadow;
    static const effectM3ElevationLight30Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationLight30OffsetX = 0;
    static const effectM3ElevationLight30OffsetY = 1;
    static const effectM3ElevationLight30Radius = 3;
    static const effectM3ElevationLight30Spread = 0;
    static const effectM3ElevationLight30Type = dropShadow;
    static const effectM3ElevationLight31Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationLight31OffsetX = 0;
    static const effectM3ElevationLight31OffsetY = 4;
    static const effectM3ElevationLight31Radius = 8;
    static const effectM3ElevationLight31Spread = 3;
    static const effectM3ElevationLight31Type = dropShadow;
    static const effectM3ElevationLight40Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationLight40OffsetX = 0;
    static const effectM3ElevationLight40OffsetY = 2;
    static const effectM3ElevationLight40Radius = 3;
    static const effectM3ElevationLight40Spread = 0;
    static const effectM3ElevationLight40Type = dropShadow;
    static const effectM3ElevationLight41Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationLight41OffsetX = 0;
    static const effectM3ElevationLight41OffsetY = 6;
    static const effectM3ElevationLight41Radius = 10;
    static const effectM3ElevationLight41Spread = 4;
    static const effectM3ElevationLight41Type = dropShadow;
    static const effectM3ElevationLight50Color = rgba(0, 0, 0, 0.3);
    static const effectM3ElevationLight50OffsetX = 0;
    static const effectM3ElevationLight50OffsetY = 4;
    static const effectM3ElevationLight50Radius = 4;
    static const effectM3ElevationLight50Spread = 0;
    static const effectM3ElevationLight50Type = dropShadow;
    static const effectM3ElevationLight51Color = rgba(0, 0, 0, 0.15);
    static const effectM3ElevationLight51OffsetX = 0;
    static const effectM3ElevationLight51OffsetY = 8;
    static const effectM3ElevationLight51Radius = 12;
    static const effectM3ElevationLight51Spread = 6;
    static const effectM3ElevationLight51Type = dropShadow;
    static const fontM3Body1FontFamily = "Roboto";
    static const fontM3Body1FontSize = "16";
    static const fontM3Body1FontStretch = "normal";
    static const fontM3Body1FontStyle = "normal";
    static const fontM3Body1FontStyleOld = "Regular";
    static const fontM3Body1FontWeight = "400";
    static const fontM3Body1LetterSpacing = "0.5";
    static const fontM3Body1LineHeight = "24";
    static const fontM3Body1ParagraphIndent = "0";
    static const fontM3Body1ParagraphSpacing = "0";
    static const fontM3Body1TextCase = "none";
    static const fontM3Body1TextDecoration = "none";
    static const fontM3Body2FontFamily = "Roboto";
    static const fontM3Body2FontSize = "14";
    static const fontM3Body2FontStretch = "normal";
    static const fontM3Body2FontStyle = "normal";
    static const fontM3Body2FontStyleOld = "Regular";
    static const fontM3Body2FontWeight = "400";
    static const fontM3Body2LetterSpacing = "0.25";
    static const fontM3Body2LineHeight = "20";
    static const fontM3Body2ParagraphIndent = "0";
    static const fontM3Body2ParagraphSpacing = "0";
    static const fontM3Body2TextCase = "none";
    static const fontM3Body2TextDecoration = "none";
    static const fontM3BodyLargeFontFamily = "Roboto";
    static const fontM3BodyLargeFontSize = "16";
    static const fontM3BodyLargeFontStretch = "normal";
    static const fontM3BodyLargeFontStyle = "normal";
    static const fontM3BodyLargeFontStyleOld = "Regular";
    static const fontM3BodyLargeFontWeight = "400";
    static const fontM3BodyLargeLetterSpacing = "0.5";
    static const fontM3BodyLargeLineHeight = "24";
    static const fontM3BodyLargeParagraphIndent = "0";
    static const fontM3BodyLargeParagraphSpacing = "0";
    static const fontM3BodyLargeTextCase = "none";
    static const fontM3BodyLargeTextDecoration = "none";
    static const fontM3BodyMediumFontFamily = "Roboto";
    static const fontM3BodyMediumFontSize = "14";
    static const fontM3BodyMediumFontStretch = "normal";
    static const fontM3BodyMediumFontStyle = "normal";
    static const fontM3BodyMediumFontStyleOld = "Regular";
    static const fontM3BodyMediumFontWeight = "400";
    static const fontM3BodyMediumLetterSpacing = "0.25";
    static const fontM3BodyMediumLineHeight = "20";
    static const fontM3BodyMediumParagraphIndent = "0";
    static const fontM3BodyMediumParagraphSpacing = "0";
    static const fontM3BodyMediumTextCase = "none";
    static const fontM3BodyMediumTextDecoration = "none";
    static const fontM3BodySmallFontFamily = "Roboto";
    static const fontM3BodySmallFontSize = "12";
    static const fontM3BodySmallFontStretch = "normal";
    static const fontM3BodySmallFontStyle = "normal";
    static const fontM3BodySmallFontStyleOld = "Regular";
    static const fontM3BodySmallFontWeight = "400";
    static const fontM3BodySmallLetterSpacing = "0.4";
    static const fontM3BodySmallLineHeight = "16";
    static const fontM3BodySmallParagraphIndent = "0";
    static const fontM3BodySmallParagraphSpacing = "0";
    static const fontM3BodySmallTextCase = "none";
    static const fontM3BodySmallTextDecoration = "none";
    static const fontM3ButtonFontFamily = "Roboto";
    static const fontM3ButtonFontSize = "14";
    static const fontM3ButtonFontStretch = "normal";
    static const fontM3ButtonFontStyle = "normal";
    static const fontM3ButtonFontStyleOld = "Medium";
    static const fontM3ButtonFontWeight = "500";
    static const fontM3ButtonLetterSpacing = "0.1";
    static const fontM3ButtonLineHeight = "20";
    static const fontM3ButtonParagraphIndent = "0";
    static const fontM3ButtonParagraphSpacing = "0";
    static const fontM3ButtonTextCase = "none";
    static const fontM3ButtonTextDecoration = "none";
    static const fontM3CaptionFontFamily = "Roboto";
    static const fontM3CaptionFontSize = "12";
    static const fontM3CaptionFontStretch = "normal";
    static const fontM3CaptionFontStyle = "normal";
    static const fontM3CaptionFontStyleOld = "Regular";
    static const fontM3CaptionFontWeight = "400";
    static const fontM3CaptionLetterSpacing = "0.4";
    static const fontM3CaptionLineHeight = "16";
    static const fontM3CaptionParagraphIndent = "0";
    static const fontM3CaptionParagraphSpacing = "0";
    static const fontM3CaptionTextCase = "none";
    static const fontM3CaptionTextDecoration = "none";
    static const fontM3Display1FontFamily = "Roboto";
    static const fontM3Display1FontSize = "64";
    static const fontM3Display1FontStretch = "normal";
    static const fontM3Display1FontStyle = "normal";
    static const fontM3Display1FontStyleOld = "Regular";
    static const fontM3Display1FontWeight = "400";
    static const fontM3Display1LetterSpacing = "-0.5";
    static const fontM3Display1LineHeight = "76";
    static const fontM3Display1ParagraphIndent = "0";
    static const fontM3Display1ParagraphSpacing = "0";
    static const fontM3Display1TextCase = "none";
    static const fontM3Display1TextDecoration = "none";
    static const fontM3Display2FontFamily = "Roboto";
    static const fontM3Display2FontSize = "57";
    static const fontM3Display2FontStretch = "normal";
    static const fontM3Display2FontStyle = "normal";
    static const fontM3Display2FontStyleOld = "Regular";
    static const fontM3Display2FontWeight = "400";
    static const fontM3Display2LetterSpacing = "-0.25";
    static const fontM3Display2LineHeight = "64";
    static const fontM3Display2ParagraphIndent = "0";
    static const fontM3Display2ParagraphSpacing = "0";
    static const fontM3Display2TextCase = "none";
    static const fontM3Display2TextDecoration = "none";
    static const fontM3Display3FontFamily = "Roboto";
    static const fontM3Display3FontSize = "45";
    static const fontM3Display3FontStretch = "normal";
    static const fontM3Display3FontStyle = "normal";
    static const fontM3Display3FontStyleOld = "Regular";
    static const fontM3Display3FontWeight = "400";
    static const fontM3Display3LetterSpacing = "0";
    static const fontM3Display3LineHeight = "52";
    static const fontM3Display3ParagraphIndent = "0";
    static const fontM3Display3ParagraphSpacing = "0";
    static const fontM3Display3TextCase = "none";
    static const fontM3Display3TextDecoration = "none";
    static const fontM3DisplayLargeFontFamily = "Roboto";
    static const fontM3DisplayLargeFontSize = "57";
    static const fontM3DisplayLargeFontStretch = "normal";
    static const fontM3DisplayLargeFontStyle = "normal";
    static const fontM3DisplayLargeFontStyleOld = "Regular";
    static const fontM3DisplayLargeFontWeight = "400";
    static const fontM3DisplayLargeLetterSpacing = "-0.25";
    static const fontM3DisplayLargeLineHeight = "64";
    static const fontM3DisplayLargeParagraphIndent = "0";
    static const fontM3DisplayLargeParagraphSpacing = "0";
    static const fontM3DisplayLargeTextCase = "none";
    static const fontM3DisplayLargeTextDecoration = "none";
    static const fontM3DisplayMediumFontFamily = "Roboto";
    static const fontM3DisplayMediumFontSize = "45";
    static const fontM3DisplayMediumFontStretch = "normal";
    static const fontM3DisplayMediumFontStyle = "normal";
    static const fontM3DisplayMediumFontStyleOld = "Regular";
    static const fontM3DisplayMediumFontWeight = "400";
    static const fontM3DisplayMediumLetterSpacing = "0";
    static const fontM3DisplayMediumLineHeight = "52";
    static const fontM3DisplayMediumParagraphIndent = "0";
    static const fontM3DisplayMediumParagraphSpacing = "0";
    static const fontM3DisplayMediumTextCase = "none";
    static const fontM3DisplayMediumTextDecoration = "none";
    static const fontM3DisplaySmallFontFamily = "Roboto";
    static const fontM3DisplaySmallFontSize = "36";
    static const fontM3DisplaySmallFontStretch = "normal";
    static const fontM3DisplaySmallFontStyle = "normal";
    static const fontM3DisplaySmallFontStyleOld = "Regular";
    static const fontM3DisplaySmallFontWeight = "400";
    static const fontM3DisplaySmallLetterSpacing = "0";
    static const fontM3DisplaySmallLineHeight = "44";
    static const fontM3DisplaySmallParagraphIndent = "0";
    static const fontM3DisplaySmallParagraphSpacing = "0";
    static const fontM3DisplaySmallTextCase = "none";
    static const fontM3DisplaySmallTextDecoration = "none";
    static const fontM3Headline1FontFamily = "Roboto";
    static const fontM3Headline1FontSize = "36";
    static const fontM3Headline1FontStretch = "normal";
    static const fontM3Headline1FontStyle = "normal";
    static const fontM3Headline1FontStyleOld = "Regular";
    static const fontM3Headline1FontWeight = "400";
    static const fontM3Headline1LetterSpacing = "0";
    static const fontM3Headline1LineHeight = "44";
    static const fontM3Headline1ParagraphIndent = "0";
    static const fontM3Headline1ParagraphSpacing = "0";
    static const fontM3Headline1TextCase = "none";
    static const fontM3Headline1TextDecoration = "none";
    static const fontM3Headline2FontFamily = "Roboto";
    static const fontM3Headline2FontSize = "32";
    static const fontM3Headline2FontStretch = "normal";
    static const fontM3Headline2FontStyle = "normal";
    static const fontM3Headline2FontStyleOld = "Regular";
    static const fontM3Headline2FontWeight = "400";
    static const fontM3Headline2LetterSpacing = "0";
    static const fontM3Headline2LineHeight = "40";
    static const fontM3Headline2ParagraphIndent = "0";
    static const fontM3Headline2ParagraphSpacing = "0";
    static const fontM3Headline2TextCase = "none";
    static const fontM3Headline2TextDecoration = "none";
    static const fontM3Headline3FontFamily = "Roboto";
    static const fontM3Headline3FontSize = "28";
    static const fontM3Headline3FontStretch = "normal";
    static const fontM3Headline3FontStyle = "normal";
    static const fontM3Headline3FontStyleOld = "Regular";
    static const fontM3Headline3FontWeight = "400";
    static const fontM3Headline3LetterSpacing = "0";
    static const fontM3Headline3LineHeight = "36";
    static const fontM3Headline3ParagraphIndent = "0";
    static const fontM3Headline3ParagraphSpacing = "0";
    static const fontM3Headline3TextCase = "none";
    static const fontM3Headline3TextDecoration = "none";
    static const fontM3Headline4FontFamily = "Roboto";
    static const fontM3Headline4FontSize = "24";
    static const fontM3Headline4FontStretch = "normal";
    static const fontM3Headline4FontStyle = "normal";
    static const fontM3Headline4FontStyleOld = "Regular";
    static const fontM3Headline4FontWeight = "400";
    static const fontM3Headline4LetterSpacing = "0";
    static const fontM3Headline4LineHeight = "32";
    static const fontM3Headline4ParagraphIndent = "0";
    static const fontM3Headline4ParagraphSpacing = "0";
    static const fontM3Headline4TextCase = "none";
    static const fontM3Headline4TextDecoration = "none";
    static const fontM3Headline5FontFamily = "Roboto";
    static const fontM3Headline5FontSize = "22";
    static const fontM3Headline5FontStretch = "normal";
    static const fontM3Headline5FontStyle = "normal";
    static const fontM3Headline5FontStyleOld = "Regular";
    static const fontM3Headline5FontWeight = "400";
    static const fontM3Headline5LetterSpacing = "0";
    static const fontM3Headline5LineHeight = "28";
    static const fontM3Headline5ParagraphIndent = "0";
    static const fontM3Headline5ParagraphSpacing = "0";
    static const fontM3Headline5TextCase = "none";
    static const fontM3Headline5TextDecoration = "none";
    static const fontM3Headline6FontFamily = "Roboto";
    static const fontM3Headline6FontSize = "18";
    static const fontM3Headline6FontStretch = "normal";
    static const fontM3Headline6FontStyle = "normal";
    static const fontM3Headline6FontStyleOld = "Regular";
    static const fontM3Headline6FontWeight = "400";
    static const fontM3Headline6LetterSpacing = "0";
    static const fontM3Headline6LineHeight = "24";
    static const fontM3Headline6ParagraphIndent = "0";
    static const fontM3Headline6ParagraphSpacing = "0";
    static const fontM3Headline6TextCase = "none";
    static const fontM3Headline6TextDecoration = "none";
    static const fontM3HeadlineLargeFontFamily = "Roboto";
    static const fontM3HeadlineLargeFontSize = "32";
    static const fontM3HeadlineLargeFontStretch = "normal";
    static const fontM3HeadlineLargeFontStyle = "normal";
    static const fontM3HeadlineLargeFontStyleOld = "Regular";
    static const fontM3HeadlineLargeFontWeight = "400";
    static const fontM3HeadlineLargeLetterSpacing = "0";
    static const fontM3HeadlineLargeLineHeight = "40";
    static const fontM3HeadlineLargeParagraphIndent = "0";
    static const fontM3HeadlineLargeParagraphSpacing = "0";
    static const fontM3HeadlineLargeTextCase = "none";
    static const fontM3HeadlineLargeTextDecoration = "none";
    static const fontM3HeadlineMediumFontFamily = "Roboto";
    static const fontM3HeadlineMediumFontSize = "28";
    static const fontM3HeadlineMediumFontStretch = "normal";
    static const fontM3HeadlineMediumFontStyle = "normal";
    static const fontM3HeadlineMediumFontStyleOld = "Regular";
    static const fontM3HeadlineMediumFontWeight = "400";
    static const fontM3HeadlineMediumLetterSpacing = "0";
    static const fontM3HeadlineMediumLineHeight = "36";
    static const fontM3HeadlineMediumParagraphIndent = "0";
    static const fontM3HeadlineMediumParagraphSpacing = "0";
    static const fontM3HeadlineMediumTextCase = "none";
    static const fontM3HeadlineMediumTextDecoration = "none";
    static const fontM3HeadlineSmallFontFamily = "Roboto";
    static const fontM3HeadlineSmallFontSize = "24";
    static const fontM3HeadlineSmallFontStretch = "normal";
    static const fontM3HeadlineSmallFontStyle = "normal";
    static const fontM3HeadlineSmallFontStyleOld = "Regular";
    static const fontM3HeadlineSmallFontWeight = "400";
    static const fontM3HeadlineSmallLetterSpacing = "0";
    static const fontM3HeadlineSmallLineHeight = "32";
    static const fontM3HeadlineSmallParagraphIndent = "0";
    static const fontM3HeadlineSmallParagraphSpacing = "0";
    static const fontM3HeadlineSmallTextCase = "none";
    static const fontM3HeadlineSmallTextDecoration = "none";
    static const fontM3LabelLargeFontFamily = "Roboto";
    static const fontM3LabelLargeFontSize = "14";
    static const fontM3LabelLargeFontStretch = "normal";
    static const fontM3LabelLargeFontStyle = "normal";
    static const fontM3LabelLargeFontStyleOld = "Medium";
    static const fontM3LabelLargeFontWeight = "500";
    static const fontM3LabelLargeLetterSpacing = "0.1";
    static const fontM3LabelLargeLineHeight = "20";
    static const fontM3LabelLargeParagraphIndent = "0";
    static const fontM3LabelLargeParagraphSpacing = "0";
    static const fontM3LabelLargeTextCase = "none";
    static const fontM3LabelLargeTextDecoration = "none";
    static const fontM3LabelMediumFontFamily = "Roboto";
    static const fontM3LabelMediumFontSize = "12";
    static const fontM3LabelMediumFontStretch = "normal";
    static const fontM3LabelMediumFontStyle = "normal";
    static const fontM3LabelMediumFontStyleOld = "Medium";
    static const fontM3LabelMediumFontWeight = "500";
    static const fontM3LabelMediumLetterSpacing = "0.5";
    static const fontM3LabelMediumLineHeight = "16";
    static const fontM3LabelMediumParagraphIndent = "0";
    static const fontM3LabelMediumParagraphSpacing = "0";
    static const fontM3LabelMediumTextCase = "none";
    static const fontM3LabelMediumTextDecoration = "none";
    static const fontM3LabelSmallFontFamily = "Roboto";
    static const fontM3LabelSmallFontSize = "11";
    static const fontM3LabelSmallFontStretch = "normal";
    static const fontM3LabelSmallFontStyle = "normal";
    static const fontM3LabelSmallFontStyleOld = "Medium";
    static const fontM3LabelSmallFontWeight = "500";
    static const fontM3LabelSmallLetterSpacing = "0.5";
    static const fontM3LabelSmallLineHeight = "16";
    static const fontM3LabelSmallParagraphIndent = "0";
    static const fontM3LabelSmallParagraphSpacing = "0";
    static const fontM3LabelSmallTextCase = "none";
    static const fontM3LabelSmallTextDecoration = "none";
    static const fontM3LabelsmallFontFamily = "Roboto";
    static const fontM3LabelsmallFontSize = "11";
    static const fontM3LabelsmallFontStretch = "normal";
    static const fontM3LabelsmallFontStyle = "normal";
    static const fontM3LabelsmallFontStyleOld = "Medium";
    static const fontM3LabelsmallFontWeight = "500";
    static const fontM3LabelsmallLetterSpacing = "0.5";
    static const fontM3LabelsmallLineHeight = "16";
    static const fontM3LabelsmallParagraphIndent = "0";
    static const fontM3LabelsmallParagraphSpacing = "0";
    static const fontM3LabelsmallTextCase = "none";
    static const fontM3LabelsmallTextDecoration = "none";
    static const fontM3OverlineFontFamily = "Roboto";
    static const fontM3OverlineFontSize = "12";
    static const fontM3OverlineFontStretch = "normal";
    static const fontM3OverlineFontStyle = "normal";
    static const fontM3OverlineFontStyleOld = "Medium";
    static const fontM3OverlineFontWeight = "500";
    static const fontM3OverlineLetterSpacing = "0.5";
    static const fontM3OverlineLineHeight = "16";
    static const fontM3OverlineParagraphIndent = "0";
    static const fontM3OverlineParagraphSpacing = "0";
    static const fontM3OverlineTextCase = "none";
    static const fontM3OverlineTextDecoration = "none";
    static const fontM3Subhead1FontFamily = "Roboto";
    static const fontM3Subhead1FontSize = "16";
    static const fontM3Subhead1FontStretch = "normal";
    static const fontM3Subhead1FontStyle = "normal";
    static const fontM3Subhead1FontStyleOld = "Medium";
    static const fontM3Subhead1FontWeight = "500";
    static const fontM3Subhead1LetterSpacing = "0.1";
    static const fontM3Subhead1LineHeight = "24";
    static const fontM3Subhead1ParagraphIndent = "0";
    static const fontM3Subhead1ParagraphSpacing = "0";
    static const fontM3Subhead1TextCase = "none";
    static const fontM3Subhead1TextDecoration = "none";
    static const fontM3Subhead2FontFamily = "Roboto";
    static const fontM3Subhead2FontSize = "14";
    static const fontM3Subhead2FontStretch = "normal";
    static const fontM3Subhead2FontStyle = "normal";
    static const fontM3Subhead2FontStyleOld = "Medium";
    static const fontM3Subhead2FontWeight = "500";
    static const fontM3Subhead2LetterSpacing = "0.1";
    static const fontM3Subhead2LineHeight = "20";
    static const fontM3Subhead2ParagraphIndent = "0";
    static const fontM3Subhead2ParagraphSpacing = "0";
    static const fontM3Subhead2TextCase = "none";
    static const fontM3Subhead2TextDecoration = "none";
    static const fontM3TitleLargeFontFamily = "Roboto";
    static const fontM3TitleLargeFontSize = "22";
    static const fontM3TitleLargeFontStretch = "normal";
    static const fontM3TitleLargeFontStyle = "normal";
    static const fontM3TitleLargeFontStyleOld = "Regular";
    static const fontM3TitleLargeFontWeight = "400";
    static const fontM3TitleLargeLetterSpacing = "0";
    static const fontM3TitleLargeLineHeight = "28";
    static const fontM3TitleLargeParagraphIndent = "0";
    static const fontM3TitleLargeParagraphSpacing = "0";
    static const fontM3TitleLargeTextCase = "none";
    static const fontM3TitleLargeTextDecoration = "none";
    static const fontM3TitleMediumFontFamily = "Roboto";
    static const fontM3TitleMediumFontSize = "16";
    static const fontM3TitleMediumFontStretch = "normal";
    static const fontM3TitleMediumFontStyle = "normal";
    static const fontM3TitleMediumFontStyleOld = "Medium";
    static const fontM3TitleMediumFontWeight = "500";
    static const fontM3TitleMediumLetterSpacing = "0.1";
    static const fontM3TitleMediumLineHeight = "24";
    static const fontM3TitleMediumParagraphIndent = "0";
    static const fontM3TitleMediumParagraphSpacing = "0";
    static const fontM3TitleMediumTextCase = "none";
    static const fontM3TitleMediumTextDecoration = "none";
    static const fontM3TitleSmallFontFamily = "Roboto";
    static const fontM3TitleSmallFontSize = "14";
    static const fontM3TitleSmallFontStretch = "normal";
    static const fontM3TitleSmallFontStyle = "normal";
    static const fontM3TitleSmallFontStyleOld = "Medium";
    static const fontM3TitleSmallFontWeight = "500";
    static const fontM3TitleSmallLetterSpacing = "0.1";
    static const fontM3TitleSmallLineHeight = "20";
    static const fontM3TitleSmallParagraphIndent = "0";
    static const fontM3TitleSmallParagraphSpacing = "0";
    static const fontM3TitleSmallTextCase = "none";
    static const fontM3TitleSmallTextDecoration = "none";
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOffAlignment = stretch;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOffCount = 4;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOffGutterSize = 16;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOffOffset = 16;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOffPattern = columns;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn0Alignment = stretch;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn0Count = 4;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn0GutterSize = 16;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn0Offset = 16;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn0Pattern = columns;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn1Alignment = max;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn1Count = 1;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn1GutterSize = 20;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn1Offset = 0;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn1Pattern = rows;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn1SectionSize = 56;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn2Alignment = min;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn2Count = 1;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn2GutterSize = 1;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn2Offset = 0;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn2Pattern = rows;
    static const gridM3ExtraSmallExtraSmall0599dpLayoutRegionsOn2SectionSize = 56;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded0Alignment = center;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded0Count = 12;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded0GutterSize = 24;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded0Pattern = columns;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded0SectionSize = 72;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded1Alignment = min;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded1Count = 1;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded1GutterSize = 1;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded1Offset = 0;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded1Pattern = columns;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded1SectionSize = 256;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded2Alignment = min;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded2Count = 1;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded2GutterSize = 1;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded2Offset = 0;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded2Pattern = rows;
    static const gridM3LargeLarge1440DpLayoutRegionsExpanded2SectionSize = 56;
    static const gridM3LargeLarge1440DpLayoutRegionsOffAlignment = center;
    static const gridM3LargeLarge1440DpLayoutRegionsOffCount = 12;
    static const gridM3LargeLarge1440DpLayoutRegionsOffGutterSize = 24;
    static const gridM3LargeLarge1440DpLayoutRegionsOffPattern = columns;
    static const gridM3LargeLarge1440DpLayoutRegionsOffSectionSize = 72;
    static const gridM3LargeLarge1440DpLayoutRegionsOn0Alignment = center;
    static const gridM3LargeLarge1440DpLayoutRegionsOn0Count = 12;
    static const gridM3LargeLarge1440DpLayoutRegionsOn0GutterSize = 24;
    static const gridM3LargeLarge1440DpLayoutRegionsOn0Pattern = columns;
    static const gridM3LargeLarge1440DpLayoutRegionsOn0SectionSize = 72;
    static const gridM3LargeLarge1440DpLayoutRegionsOn1Alignment = min;
    static const gridM3LargeLarge1440DpLayoutRegionsOn1Count = 1;
    static const gridM3LargeLarge1440DpLayoutRegionsOn1GutterSize = 1;
    static const gridM3LargeLarge1440DpLayoutRegionsOn1Offset = 0;
    static const gridM3LargeLarge1440DpLayoutRegionsOn1Pattern = columns;
    static const gridM3LargeLarge1440DpLayoutRegionsOn1SectionSize = 72;
    static const gridM3LargeLarge1440DpLayoutRegionsOn2Alignment = min;
    static const gridM3LargeLarge1440DpLayoutRegionsOn2Count = 1;
    static const gridM3LargeLarge1440DpLayoutRegionsOn2GutterSize = 1;
    static const gridM3LargeLarge1440DpLayoutRegionsOn2Offset = 0;
    static const gridM3LargeLarge1440DpLayoutRegionsOn2Pattern = rows;
    static const gridM3LargeLarge1440DpLayoutRegionsOn2SectionSize = 56;
    static const gridM3MediumMedium12401439dpLayoutRegionsOffAlignment = stretch;
    static const gridM3MediumMedium12401439dpLayoutRegionsOffCount = 12;
    static const gridM3MediumMedium12401439dpLayoutRegionsOffGutterSize = 24;
    static const gridM3MediumMedium12401439dpLayoutRegionsOffOffset = 200;
    static const gridM3MediumMedium12401439dpLayoutRegionsOffPattern = columns;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn0Alignment = stretch;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn0Count = 12;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn0GutterSize = 24;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn0Offset = 200;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn0Pattern = columns;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn1Alignment = min;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn1Count = 1;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn1GutterSize = 1;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn1Offset = 0;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn1Pattern = columns;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn1SectionSize = 72;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn2Alignment = min;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn2Count = 1;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn2GutterSize = 1;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn2Offset = 0;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn2Pattern = rows;
    static const gridM3MediumMedium12401439dpLayoutRegionsOn2SectionSize = 56;
    static const gridM3MediumMedium9051239dpLayoutRegionsOffAlignment = stretch;
    static const gridM3MediumMedium9051239dpLayoutRegionsOffCount = 12;
    static const gridM3MediumMedium9051239dpLayoutRegionsOffGutterSize = 24;
    static const gridM3MediumMedium9051239dpLayoutRegionsOffOffset = 24;
    static const gridM3MediumMedium9051239dpLayoutRegionsOffPattern = columns;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn0Alignment = max;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn0Count = 12;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn0GutterSize = 12;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn0Offset = 32;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn0Pattern = columns;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn0SectionSize = 52;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn1Alignment = min;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn1Count = 1;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn1GutterSize = 1;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn1Offset = 0;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn1Pattern = columns;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn1SectionSize = 72;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn2Alignment = min;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn2Count = 1;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn2GutterSize = 1;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn2Offset = 0;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn2Pattern = rows;
    static const gridM3MediumMedium9051239dpLayoutRegionsOn2SectionSize = 56;
    static const gridM3SmallSmall600904dpLayoutRegionsOffAlignment = stretch;
    static const gridM3SmallSmall600904dpLayoutRegionsOffCount = 8;
    static const gridM3SmallSmall600904dpLayoutRegionsOffGutterSize = 16;
    static const gridM3SmallSmall600904dpLayoutRegionsOffOffset = 32;
    static const gridM3SmallSmall600904dpLayoutRegionsOffPattern = columns;
    static const gridM3SmallSmall600904dpLayoutRegionsOn0Alignment = max;
    static const gridM3SmallSmall600904dpLayoutRegionsOn0Count = 8;
    static const gridM3SmallSmall600904dpLayoutRegionsOn0GutterSize = 16;
    static const gridM3SmallSmall600904dpLayoutRegionsOn0Offset = 32;
    static const gridM3SmallSmall600904dpLayoutRegionsOn0Pattern = columns;
    static const gridM3SmallSmall600904dpLayoutRegionsOn0SectionSize = 46;
    static const gridM3SmallSmall600904dpLayoutRegionsOn1Alignment = min;
    static const gridM3SmallSmall600904dpLayoutRegionsOn1Count = 1;
    static const gridM3SmallSmall600904dpLayoutRegionsOn1GutterSize = 1;
    static const gridM3SmallSmall600904dpLayoutRegionsOn1Offset = 0;
    static const gridM3SmallSmall600904dpLayoutRegionsOn1Pattern = columns;
    static const gridM3SmallSmall600904dpLayoutRegionsOn1SectionSize = 72;
    static const gridM3SmallSmall600904dpLayoutRegionsOn2Alignment = min;
    static const gridM3SmallSmall600904dpLayoutRegionsOn2Count = 1;
    static const gridM3SmallSmall600904dpLayoutRegionsOn2GutterSize = 1;
    static const gridM3SmallSmall600904dpLayoutRegionsOn2Offset = 0;
    static const gridM3SmallSmall600904dpLayoutRegionsOn2Pattern = rows;
    static const gridM3SmallSmall600904dpLayoutRegionsOn2SectionSize = 56;
}