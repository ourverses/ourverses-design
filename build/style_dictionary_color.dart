
//
// style_dictionary_color.dart
//

// Do not edit directly
// Generated on Mon, 17 Jan 2022 17:06:35 GMT



import 'dart:ui';

class StyleDictionaryColor {
  StyleDictionaryColor._();

    static const m3ReadOnlyDarkBackgroundOpacity008 = Color(0x141C1B1F);
    static const m3ReadOnlyDarkBackgroundOpacity012 = Color(0x1F1C1B1F);
    static const m3ReadOnlyDarkBackgroundOpacity016 = Color(0x291C1B1F);
    static const m3ReadOnlyDarkBlack = Color(0xFF000000);
    static const m3ReadOnlyDarkErrorContainerOpacity008 = Color(0x148C1D18);
    static const m3ReadOnlyDarkErrorContainerOpacity012 = Color(0x1F8C1D18);
    static const m3ReadOnlyDarkErrorContainerOpacity016 = Color(0x298C1D18);
    static const m3ReadOnlyDarkErrorOpacity008 = Color(0x14F2B8B5);
    static const m3ReadOnlyDarkErrorOpacity012 = Color(0x1FF2B8B5);
    static const m3ReadOnlyDarkErrorOpacity016 = Color(0x29F2B8B5);
    static const m3ReadOnlyDarkInverseOnSurfaceOpacity008 = Color(0x141C1B1F);
    static const m3ReadOnlyDarkInverseOnSurfaceOpacity012 = Color(0x1F1C1B1F);
    static const m3ReadOnlyDarkInverseOnSurfaceOpacity016 = Color(0x291C1B1F);
    static const m3ReadOnlyDarkInverseSurfaceOpacity008 = Color(0x14E6E1E5);
    static const m3ReadOnlyDarkInverseSurfaceOpacity012 = Color(0x1FE6E1E5);
    static const m3ReadOnlyDarkInverseSurfaceOpacity016 = Color(0x29E6E1E5);
    static const m3ReadOnlyDarkOnBackgroundOpacity008 = Color(0x14E6E1E5);
    static const m3ReadOnlyDarkOnBackgroundOpacity012 = Color(0x1FE6E1E5);
    static const m3ReadOnlyDarkOnBackgroundOpacity016 = Color(0x29E6E1E5);
    static const m3ReadOnlyDarkOnErrorContainerOpacity008 = Color(0x14F9DEDC);
    static const m3ReadOnlyDarkOnErrorContainerOpacity012 = Color(0x1FF9DEDC);
    static const m3ReadOnlyDarkOnErrorContainerOpacity016 = Color(0x29F9DEDC);
    static const m3ReadOnlyDarkOnErrorOpacity008 = Color(0x14601410);
    static const m3ReadOnlyDarkOnErrorOpacity012 = Color(0x1F601410);
    static const m3ReadOnlyDarkOnErrorOpacity016 = Color(0x29601410);
    static const m3ReadOnlyDarkOnPrimaryContainerOpacity008 = Color(0x14EADDFF);
    static const m3ReadOnlyDarkOnPrimaryContainerOpacity012 = Color(0x1FEADDFF);
    static const m3ReadOnlyDarkOnPrimaryContainerOpacity016 = Color(0x29EADDFF);
    static const m3ReadOnlyDarkOnPrimaryOpacity008 = Color(0x14381E72);
    static const m3ReadOnlyDarkOnPrimaryOpacity012 = Color(0x1F381E72);
    static const m3ReadOnlyDarkOnPrimaryOpacity016 = Color(0x29381E72);
    static const m3ReadOnlyDarkOnSecondaryContainerOpacity008 = Color(0x14E8DEF8);
    static const m3ReadOnlyDarkOnSecondaryContainerOpacity012 = Color(0x1FE8DEF8);
    static const m3ReadOnlyDarkOnSecondaryContainerOpacity016 = Color(0x29E8DEF8);
    static const m3ReadOnlyDarkOnSecondaryOpacity008 = Color(0x14332D41);
    static const m3ReadOnlyDarkOnSecondaryOpacity012 = Color(0x1F332D41);
    static const m3ReadOnlyDarkOnSecondaryOpacity016 = Color(0x29332D41);
    static const m3ReadOnlyDarkOnSurfaceOpacity008 = Color(0x14E6E1E5);
    static const m3ReadOnlyDarkOnSurfaceOpacity012 = Color(0x1FE6E1E5);
    static const m3ReadOnlyDarkOnSurfaceOpacity016 = Color(0x29E6E1E5);
    static const m3ReadOnlyDarkOnSurfaceVariantOpacity008 = Color(0x14CAC4D0);
    static const m3ReadOnlyDarkOnSurfaceVariantOpacity012 = Color(0x1FCAC4D0);
    static const m3ReadOnlyDarkOnSurfaceVariantOpacity016 = Color(0x29CAC4D0);
    static const m3ReadOnlyDarkOnTertiaryContainerOpacity008 = Color(0x14FFD8E4);
    static const m3ReadOnlyDarkOnTertiaryContainerOpacity012 = Color(0x1FFFD8E4);
    static const m3ReadOnlyDarkOnTertiaryContainerOpacity016 = Color(0x29FFD8E4);
    static const m3ReadOnlyDarkOnTertiaryOpacity008 = Color(0x14492532);
    static const m3ReadOnlyDarkOnTertiaryOpacity012 = Color(0x1F492532);
    static const m3ReadOnlyDarkOnTertiaryOpacity016 = Color(0x29492532);
    static const m3ReadOnlyDarkOutlineOpacity008 = Color(0x14938F99);
    static const m3ReadOnlyDarkOutlineOpacity012 = Color(0x1F938F99);
    static const m3ReadOnlyDarkOutlineOpacity016 = Color(0x29938F99);
    static const m3ReadOnlyDarkPrimaryContainerOpacity008 = Color(0x144F378B);
    static const m3ReadOnlyDarkPrimaryContainerOpacity012 = Color(0x1F4F378B);
    static const m3ReadOnlyDarkPrimaryContainerOpacity016 = Color(0x294F378B);
    static const m3ReadOnlyDarkPrimaryOpacity008 = Color(0x14D0BCFF);
    static const m3ReadOnlyDarkPrimaryOpacity012 = Color(0x1FD0BCFF);
    static const m3ReadOnlyDarkPrimaryOpacity016 = Color(0x29D0BCFF);
    static const m3ReadOnlyDarkSecondaryContainerOpacity008 = Color(0x144A4458);
    static const m3ReadOnlyDarkSecondaryContainerOpacity012 = Color(0x1F4A4458);
    static const m3ReadOnlyDarkSecondaryContainerOpacity016 = Color(0x294A4458);
    static const m3ReadOnlyDarkSecondaryOpacity008 = Color(0x14CCC2DC);
    static const m3ReadOnlyDarkSecondaryOpacity012 = Color(0x1FCCC2DC);
    static const m3ReadOnlyDarkSecondaryOpacity016 = Color(0x29CCC2DC);
    static const m3ReadOnlyDarkSurface10 = Color(0xFF1C1B1F);
    static const m3ReadOnlyDarkSurface11 = Color(0x0DD0BCFF);
    static const m3ReadOnlyDarkSurface20 = Color(0xFF1C1B1F);
    static const m3ReadOnlyDarkSurface21 = Color(0x14D0BCFF);
    static const m3ReadOnlyDarkSurface30 = Color(0xFF1C1B1F);
    static const m3ReadOnlyDarkSurface31 = Color(0x1CD0BCFF);
    static const m3ReadOnlyDarkSurface40 = Color(0xFF1C1B1F);
    static const m3ReadOnlyDarkSurface41 = Color(0x1FD0BCFF);
    static const m3ReadOnlyDarkSurface50 = Color(0xFF1C1B1F);
    static const m3ReadOnlyDarkSurface51 = Color(0x24D0BCFF);
    static const m3ReadOnlyDarkSurfaceOpacity008 = Color(0x141C1B1F);
    static const m3ReadOnlyDarkSurfaceOpacity012 = Color(0x1F1C1B1F);
    static const m3ReadOnlyDarkSurfaceOpacity016 = Color(0x291C1B1F);
    static const m3ReadOnlyDarkSurfaceVariantOpacity008 = Color(0x1449454F);
    static const m3ReadOnlyDarkSurfaceVariantOpacity012 = Color(0x1F49454F);
    static const m3ReadOnlyDarkSurfaceVariantOpacity016 = Color(0x2949454F);
    static const m3ReadOnlyDarkTertiaryContainerOpacity008 = Color(0x14633B48);
    static const m3ReadOnlyDarkTertiaryContainerOpacity012 = Color(0x1F633B48);
    static const m3ReadOnlyDarkTertiaryContainerOpacity016 = Color(0x29633B48);
    static const m3ReadOnlyDarkTertiaryOpacity008 = Color(0x14EFB8C8);
    static const m3ReadOnlyDarkTertiaryOpacity012 = Color(0x1FEFB8C8);
    static const m3ReadOnlyDarkTertiaryOpacity016 = Color(0x29EFB8C8);
    static const m3ReadOnlyDarkWhite = Color(0xFFFFFFFF);
    static const m3ReadOnlyLightBackgroundOpacity008 = Color(0x14FFFBFE);
    static const m3ReadOnlyLightBackgroundOpacity012 = Color(0x1FFFFBFE);
    static const m3ReadOnlyLightBackgroundOpacity016 = Color(0x29FFFBFE);
    static const m3ReadOnlyLightBlack = Color(0xFF000000);
    static const m3ReadOnlyLightErrorContainerOpacity008 = Color(0x14F9DEDC);
    static const m3ReadOnlyLightErrorContainerOpacity012 = Color(0x1FF9DEDC);
    static const m3ReadOnlyLightErrorContainerOpacity016 = Color(0x29F9DEDC);
    static const m3ReadOnlyLightErrorOpacity008 = Color(0x14B3261E);
    static const m3ReadOnlyLightErrorOpacity012 = Color(0x1FB3261E);
    static const m3ReadOnlyLightErrorOpacity016 = Color(0x29B3261E);
    static const m3ReadOnlyLightInverseOnSurfaceOpacity008 = Color(0x14F4EFF4);
    static const m3ReadOnlyLightInverseOnSurfaceOpacity012 = Color(0x1FF4EFF4);
    static const m3ReadOnlyLightInverseOnSurfaceOpacity016 = Color(0x29F4EFF4);
    static const m3ReadOnlyLightInverseSurfaceOpacity008 = Color(0x14313033);
    static const m3ReadOnlyLightInverseSurfaceOpacity012 = Color(0x1F313033);
    static const m3ReadOnlyLightInverseSurfaceOpacity016 = Color(0x29313033);
    static const m3ReadOnlyLightOnBackgroundOpacity008 = Color(0x141C1B1F);
    static const m3ReadOnlyLightOnBackgroundOpacity012 = Color(0x1F1C1B1F);
    static const m3ReadOnlyLightOnBackgroundOpacity016 = Color(0x291C1B1F);
    static const m3ReadOnlyLightOnErrorContainerOpacity008 = Color(0x14410E0B);
    static const m3ReadOnlyLightOnErrorContainerOpacity012 = Color(0x1F410E0B);
    static const m3ReadOnlyLightOnErrorContainerOpacity016 = Color(0x29410E0B);
    static const m3ReadOnlyLightOnErrorOpacity008 = Color(0x14FFFFFF);
    static const m3ReadOnlyLightOnErrorOpacity012 = Color(0x1FFFFFFF);
    static const m3ReadOnlyLightOnErrorOpacity016 = Color(0x29FFFFFF);
    static const m3ReadOnlyLightOnPrimaryContainerOpacity008 = Color(0x1421005D);
    static const m3ReadOnlyLightOnPrimaryContainerOpacity012 = Color(0x1F21005D);
    static const m3ReadOnlyLightOnPrimaryContainerOpacity016 = Color(0x2921005D);
    static const m3ReadOnlyLightOnPrimaryOpacity008 = Color(0x14FFFFFF);
    static const m3ReadOnlyLightOnPrimaryOpacity012 = Color(0x1FFFFFFF);
    static const m3ReadOnlyLightOnPrimaryOpacity016 = Color(0x29FFFFFF);
    static const m3ReadOnlyLightOnSecondaryContainerOpacity008 = Color(0x141D192B);
    static const m3ReadOnlyLightOnSecondaryContainerOpacity012 = Color(0x1F1D192B);
    static const m3ReadOnlyLightOnSecondaryContainerOpacity016 = Color(0x291D192B);
    static const m3ReadOnlyLightOnSecondaryOpacity008 = Color(0x14FFFFFF);
    static const m3ReadOnlyLightOnSecondaryOpacity012 = Color(0x1FFFFFFF);
    static const m3ReadOnlyLightOnSecondaryOpacity016 = Color(0x29FFFFFF);
    static const m3ReadOnlyLightOnSurfaceOpacity008 = Color(0x141C1B1F);
    static const m3ReadOnlyLightOnSurfaceOpacity012 = Color(0x1F1C1B1F);
    static const m3ReadOnlyLightOnSurfaceOpacity016 = Color(0x291C1B1F);
    static const m3ReadOnlyLightOnSurfaceVariantOpacity008 = Color(0x1449454F);
    static const m3ReadOnlyLightOnSurfaceVariantOpacity012 = Color(0x1F49454F);
    static const m3ReadOnlyLightOnSurfaceVariantOpacity016 = Color(0x2949454F);
    static const m3ReadOnlyLightOnTertiaryContainerOpacity008 = Color(0x1431111D);
    static const m3ReadOnlyLightOnTertiaryContainerOpacity012 = Color(0x1F31111D);
    static const m3ReadOnlyLightOnTertiaryContainerOpacity016 = Color(0x2931111D);
    static const m3ReadOnlyLightOnTertiaryOpacity008 = Color(0x14FFFFFF);
    static const m3ReadOnlyLightOnTertiaryOpacity012 = Color(0x1FFFFFFF);
    static const m3ReadOnlyLightOnTertiaryOpacity016 = Color(0x29FFFFFF);
    static const m3ReadOnlyLightOutlineOpacity008 = Color(0x1479747E);
    static const m3ReadOnlyLightOutlineOpacity012 = Color(0x1F79747E);
    static const m3ReadOnlyLightOutlineOpacity016 = Color(0x2979747E);
    static const m3ReadOnlyLightPrimaryContainerOpacity008 = Color(0x14EADDFF);
    static const m3ReadOnlyLightPrimaryContainerOpacity012 = Color(0x1FEADDFF);
    static const m3ReadOnlyLightPrimaryContainerOpacity016 = Color(0x29EADDFF);
    static const m3ReadOnlyLightPrimaryOpacity008 = Color(0x146750A4);
    static const m3ReadOnlyLightPrimaryOpacity012 = Color(0x1F6750A4);
    static const m3ReadOnlyLightPrimaryOpacity016 = Color(0x296750A4);
    static const m3ReadOnlyLightSecondaryContainerOpacity008 = Color(0x14E8DEF8);
    static const m3ReadOnlyLightSecondaryContainerOpacity012 = Color(0x1FE8DEF8);
    static const m3ReadOnlyLightSecondaryContainerOpacity016 = Color(0x29E8DEF8);
    static const m3ReadOnlyLightSecondaryOpacity008 = Color(0x14625B71);
    static const m3ReadOnlyLightSecondaryOpacity012 = Color(0x1F625B71);
    static const m3ReadOnlyLightSecondaryOpacity016 = Color(0x29625B71);
    static const m3ReadOnlyLightSurface10 = Color(0xFFFFFBFE);
    static const m3ReadOnlyLightSurface11 = Color(0x0D6750A4);
    static const m3ReadOnlyLightSurface20 = Color(0xFFFFFBFE);
    static const m3ReadOnlyLightSurface21 = Color(0x146750A4);
    static const m3ReadOnlyLightSurface30 = Color(0xFFFFFBFE);
    static const m3ReadOnlyLightSurface31 = Color(0x1C6750A4);
    static const m3ReadOnlyLightSurface40 = Color(0xFFFFFBFE);
    static const m3ReadOnlyLightSurface41 = Color(0x1F6750A4);
    static const m3ReadOnlyLightSurface50 = Color(0xFFFFFBFE);
    static const m3ReadOnlyLightSurface51 = Color(0x246750A4);
    static const m3ReadOnlyLightSurfaceOpacity008 = Color(0x14FFFBFE);
    static const m3ReadOnlyLightSurfaceOpacity012 = Color(0x1FFFFBFE);
    static const m3ReadOnlyLightSurfaceOpacity016 = Color(0x29FFFBFE);
    static const m3ReadOnlyLightSurfaceVariantOpacity008 = Color(0x14E7E0EC);
    static const m3ReadOnlyLightSurfaceVariantOpacity012 = Color(0x1FE7E0EC);
    static const m3ReadOnlyLightSurfaceVariantOpacity016 = Color(0x29E7E0EC);
    static const m3ReadOnlyLightTertiaryContainerOpacity008 = Color(0x14FFD8E4);
    static const m3ReadOnlyLightTertiaryContainerOpacity012 = Color(0x1FFFD8E4);
    static const m3ReadOnlyLightTertiaryContainerOpacity016 = Color(0x29FFD8E4);
    static const m3ReadOnlyLightTertiaryOpacity008 = Color(0x147D5260);
    static const m3ReadOnlyLightTertiaryOpacity012 = Color(0x1F7D5260);
    static const m3ReadOnlyLightTertiaryOpacity016 = Color(0x297D5260);
    static const m3ReadOnlyLightWhite = Color(0xFFFFFFFF);
    static const m3RefErrorError0 = Color(0xFF000000);
    static const m3RefErrorError10 = Color(0xFF410E0B);
    static const m3RefErrorError100 = Color(0xFFFFFFFF);
    static const m3RefErrorError20 = Color(0xFF601410);
    static const m3RefErrorError30 = Color(0xFF8C1D18);
    static const m3RefErrorError40 = Color(0xFFB3261E);
    static const m3RefErrorError50 = Color(0xFFDC362E);
    static const m3RefErrorError60 = Color(0xFFE46962);
    static const m3RefErrorError70 = Color(0xFFEC928E);
    static const m3RefErrorError80 = Color(0xFFF2B8B5);
    static const m3RefErrorError90 = Color(0xFFF9DEDC);
    static const m3RefErrorError95 = Color(0xFFFCEEEE);
    static const m3RefErrorError99 = Color(0xFFFFFBF9);
    static const m3RefNeutralNeutral0 = Color(0xFF000000);
    static const m3RefNeutralNeutral10 = Color(0xFF1C1B1F);
    static const m3RefNeutralNeutral100 = Color(0xFFFFFFFF);
    static const m3RefNeutralNeutral20 = Color(0xFF313033);
    static const m3RefNeutralNeutral30 = Color(0xFF484649);
    static const m3RefNeutralNeutral40 = Color(0xFF605D62);
    static const m3RefNeutralNeutral50 = Color(0xFF787579);
    static const m3RefNeutralNeutral60 = Color(0xFF939094);
    static const m3RefNeutralNeutral70 = Color(0xFFAEAAAE);
    static const m3RefNeutralNeutral80 = Color(0xFFC9C5CA);
    static const m3RefNeutralNeutral90 = Color(0xFFE6E1E5);
    static const m3RefNeutralNeutral95 = Color(0xFFF4EFF4);
    static const m3RefNeutralNeutral99 = Color(0xFFFFFBFE);
    static const m3RefNeutralVariantNeutralVariant0 = Color(0xFF000000);
    static const m3RefNeutralVariantNeutralVariant10 = Color(0xFF1D1A22);
    static const m3RefNeutralVariantNeutralVariant100 = Color(0xFFFFFFFF);
    static const m3RefNeutralVariantNeutralVariant20 = Color(0xFF322F37);
    static const m3RefNeutralVariantNeutralVariant30 = Color(0xFF49454F);
    static const m3RefNeutralVariantNeutralVariant40 = Color(0xFF605D66);
    static const m3RefNeutralVariantNeutralVariant50 = Color(0xFF79747E);
    static const m3RefNeutralVariantNeutralVariant60 = Color(0xFF938F99);
    static const m3RefNeutralVariantNeutralVariant70 = Color(0xFFAEA9B4);
    static const m3RefNeutralVariantNeutralVariant80 = Color(0xFFCAC4D0);
    static const m3RefNeutralVariantNeutralVariant90 = Color(0xFFE7E0EC);
    static const m3RefNeutralVariantNeutralVariant95 = Color(0xFFF5EEFA);
    static const m3RefNeutralVariantNeutralVariant99 = Color(0xFFFFFBFE);
    static const m3RefPrimaryPrimary0 = Color(0xFF000000);
    static const m3RefPrimaryPrimary10 = Color(0xFF21005D);
    static const m3RefPrimaryPrimary100 = Color(0xFFFFFFFF);
    static const m3RefPrimaryPrimary20 = Color(0xFF381E72);
    static const m3RefPrimaryPrimary30 = Color(0xFF4F378B);
    static const m3RefPrimaryPrimary40 = Color(0xFF6750A4);
    static const m3RefPrimaryPrimary50 = Color(0xFF7F67BE);
    static const m3RefPrimaryPrimary60 = Color(0xFF9A82DB);
    static const m3RefPrimaryPrimary70 = Color(0xFFB69DF8);
    static const m3RefPrimaryPrimary80 = Color(0xFFD0BCFF);
    static const m3RefPrimaryPrimary90 = Color(0xFFEADDFF);
    static const m3RefPrimaryPrimary95 = Color(0xFFF6EDFF);
    static const m3RefPrimaryPrimary99 = Color(0xFFFFFBFE);
    static const m3RefSecondarySecondary0 = Color(0xFF000000);
    static const m3RefSecondarySecondary10 = Color(0xFF1D192B);
    static const m3RefSecondarySecondary100 = Color(0xFFFFFFFF);
    static const m3RefSecondarySecondary20 = Color(0xFF332D41);
    static const m3RefSecondarySecondary30 = Color(0xFF4A4458);
    static const m3RefSecondarySecondary40 = Color(0xFF625B71);
    static const m3RefSecondarySecondary50 = Color(0xFF7A7289);
    static const m3RefSecondarySecondary60 = Color(0xFF958DA5);
    static const m3RefSecondarySecondary70 = Color(0xFFB0A7C0);
    static const m3RefSecondarySecondary80 = Color(0xFFCCC2DC);
    static const m3RefSecondarySecondary90 = Color(0xFFE8DEF8);
    static const m3RefSecondarySecondary95 = Color(0xFFF6EDFF);
    static const m3RefSecondarySecondary99 = Color(0xFFFFFBFE);
    static const m3RefTertiaryTertiary0 = Color(0xFF000000);
    static const m3RefTertiaryTertiary10 = Color(0xFF31111D);
    static const m3RefTertiaryTertiary100 = Color(0xFFFFFFFF);
    static const m3RefTertiaryTertiary20 = Color(0xFF492532);
    static const m3RefTertiaryTertiary30 = Color(0xFF633B48);
    static const m3RefTertiaryTertiary40 = Color(0xFF7D5260);
    static const m3RefTertiaryTertiary50 = Color(0xFF986977);
    static const m3RefTertiaryTertiary60 = Color(0xFFB58392);
    static const m3RefTertiaryTertiary70 = Color(0xFFD29DAC);
    static const m3RefTertiaryTertiary80 = Color(0xFFEFB8C8);
    static const m3RefTertiaryTertiary90 = Color(0xFFFFD8E4);
    static const m3RefTertiaryTertiary95 = Color(0xFFFFECF1);
    static const m3RefTertiaryTertiary99 = Color(0xFFFFFBFA);
    static const m3SourceError = Color(0xFFB3261E);
    static const m3SourceNeutral = Color(0xFF605D62);
    static const m3SourceNeutralVariant = Color(0xFF605D66);
    static const m3SourcePrimary = Color(0xFF6750A4);
    static const m3SourceSecondary = Color(0xFF625B71);
    static const m3SourceSeed = Color(0xFF6750A4);
    static const m3SourceTertiary = Color(0xFF7D5260);
    static const m3SysDarkBackground = Color(0xFF1C1B1F); /* background : on-background

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkError = Color(0xFFF2B8B5); /* error : on-error

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkErrorContainer = Color(0xFF8C1D18); /* error-container : on-error-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkInverseOnSurface = Color(0xFF1C1B1F);
    static const m3SysDarkInverseSurface = Color(0xFFE6E1E5);
    static const m3SysDarkOnBackground = Color(0xFFE6E1E5);
    static const m3SysDarkOnError = Color(0xFF601410);
    static const m3SysDarkOnErrorContainer = Color(0xFFF9DEDC);
    static const m3SysDarkOnPrimary = Color(0xFF381E72);
    static const m3SysDarkOnPrimaryContainer = Color(0xFFEADDFF);
    static const m3SysDarkOnSecondary = Color(0xFF332D41);
    static const m3SysDarkOnSecondaryContainer = Color(0xFFE8DEF8);
    static const m3SysDarkOnSurface = Color(0xFFE6E1E5);
    static const m3SysDarkOnSurfaceVariant = Color(0xFFCAC4D0);
    static const m3SysDarkOnTertiary = Color(0xFF492532);
    static const m3SysDarkOnTertiaryContainer = Color(0xFFFFD8E4);
    static const m3SysDarkOutline = Color(0xFF938F99);
    static const m3SysDarkPrimary = Color(0xFFD0BCFF); /* primary : on-primary

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkPrimaryContainer = Color(0xFF4F378B); /* primary-container : on-primary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkSecondary = Color(0xFFCCC2DC); /* secondary : on-secondary

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkSecondaryContainer = Color(0xFF4A4458); /* secondary-container : on-secondary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkSurface = Color(0xFF1C1B1F); /* surface : on-surface

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkSurfaceVariant = Color(0xFF49454F); /* surface-variant : on-surface-variant

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkTertiary = Color(0xFFEFB8C8); /* tertiary : on-tertiary

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysDarkTertiaryContainer = Color(0xFF633B48); /* tertiary-container : on-tertiary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightBackground = Color(0xFFFFFBFE); /* background : on-background

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightError = Color(0xFFB3261E); /* error : on-error

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightErrorContainer = Color(0xFFF9DEDC); /* error-container : on-error-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightInverseOnSurface = Color(0xFFF4EFF4);
    static const m3SysLightInverseSurface = Color(0xFF313033);
    static const m3SysLightOnBackground = Color(0xFF1C1B1F);
    static const m3SysLightOnError = Color(0xFFFFFFFF);
    static const m3SysLightOnErrorContainer = Color(0xFF410E0B);
    static const m3SysLightOnPrimary = Color(0xFFFFFFFF);
    static const m3SysLightOnPrimaryContainer = Color(0xFF21005D);
    static const m3SysLightOnSecondary = Color(0xFFFFFFFF);
    static const m3SysLightOnSecondaryContainer = Color(0xFF1D192B);
    static const m3SysLightOnSurface = Color(0xFF1C1B1F);
    static const m3SysLightOnSurfaceVariant = Color(0xFF49454F);
    static const m3SysLightOnTertiary = Color(0xFFFFFFFF);
    static const m3SysLightOnTertiaryContainer = Color(0xFF31111D);
    static const m3SysLightOutline = Color(0xFF79747E);
    static const m3SysLightPrimary = Color(0xFF6750A4); /* primary : on-primary

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightPrimaryContainer = Color(0xFFEADDFF); /* primary-container : on-primary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightSecondary = Color(0xFF625B71); /* secondary : on-secondary

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightSecondaryContainer = Color(0xFFE8DEF8); /* secondary-container : on-secondary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightSurface = Color(0xFFFFFBFE); /* surface : on-surface

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightSurfaceVariant = Color(0xFFE7E0EC); /* surface-variant : on-surface-variant

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightTertiary = Color(0xFF7D5260); /* tertiary : on-tertiary

Regular text:
Level AA - ✅ Pass
Level AAA - ❌ Fail

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
    static const m3SysLightTertiaryContainer = Color(0xFFFFD8E4); /* tertiary-container : on-tertiary-container

Regular text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Large text:
Level AA - ✅ Pass
Level AAA - ✅ Pass

Icons:
Level AA - ✅ Pass
 */
}