
//
// style_dictionary_grid.dart
//

// Do not edit directly
// Generated on Mon, 17 Jan 2022 17:06:35 GMT



import 'dart:ui';

class StyleDictionaryGrid {
  StyleDictionaryGrid._();

    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOffAlignment = stretch;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOffCount = 4;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOffGutterSize = 16;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOffOffset = 16;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOffPattern = columns;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn0Alignment = stretch;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn0Count = 4;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn0GutterSize = 16;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn0Offset = 16;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn0Pattern = columns;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn1Alignment = max;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn1Count = 1;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn1GutterSize = 20;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn1Offset = 0;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn1Pattern = rows;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn1SectionSize = 56;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn2Alignment = min;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn2Count = 1;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn2GutterSize = 1;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn2Offset = 0;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn2Pattern = rows;
    static const m3ExtraSmallExtraSmall0599dpLayoutRegionsOn2SectionSize = 56;
    static const m3LargeLarge1440DpLayoutRegionsExpanded0Alignment = center;
    static const m3LargeLarge1440DpLayoutRegionsExpanded0Count = 12;
    static const m3LargeLarge1440DpLayoutRegionsExpanded0GutterSize = 24;
    static const m3LargeLarge1440DpLayoutRegionsExpanded0Pattern = columns;
    static const m3LargeLarge1440DpLayoutRegionsExpanded0SectionSize = 72;
    static const m3LargeLarge1440DpLayoutRegionsExpanded1Alignment = min;
    static const m3LargeLarge1440DpLayoutRegionsExpanded1Count = 1;
    static const m3LargeLarge1440DpLayoutRegionsExpanded1GutterSize = 1;
    static const m3LargeLarge1440DpLayoutRegionsExpanded1Offset = 0;
    static const m3LargeLarge1440DpLayoutRegionsExpanded1Pattern = columns;
    static const m3LargeLarge1440DpLayoutRegionsExpanded1SectionSize = 256;
    static const m3LargeLarge1440DpLayoutRegionsExpanded2Alignment = min;
    static const m3LargeLarge1440DpLayoutRegionsExpanded2Count = 1;
    static const m3LargeLarge1440DpLayoutRegionsExpanded2GutterSize = 1;
    static const m3LargeLarge1440DpLayoutRegionsExpanded2Offset = 0;
    static const m3LargeLarge1440DpLayoutRegionsExpanded2Pattern = rows;
    static const m3LargeLarge1440DpLayoutRegionsExpanded2SectionSize = 56;
    static const m3LargeLarge1440DpLayoutRegionsOffAlignment = center;
    static const m3LargeLarge1440DpLayoutRegionsOffCount = 12;
    static const m3LargeLarge1440DpLayoutRegionsOffGutterSize = 24;
    static const m3LargeLarge1440DpLayoutRegionsOffPattern = columns;
    static const m3LargeLarge1440DpLayoutRegionsOffSectionSize = 72;
    static const m3LargeLarge1440DpLayoutRegionsOn0Alignment = center;
    static const m3LargeLarge1440DpLayoutRegionsOn0Count = 12;
    static const m3LargeLarge1440DpLayoutRegionsOn0GutterSize = 24;
    static const m3LargeLarge1440DpLayoutRegionsOn0Pattern = columns;
    static const m3LargeLarge1440DpLayoutRegionsOn0SectionSize = 72;
    static const m3LargeLarge1440DpLayoutRegionsOn1Alignment = min;
    static const m3LargeLarge1440DpLayoutRegionsOn1Count = 1;
    static const m3LargeLarge1440DpLayoutRegionsOn1GutterSize = 1;
    static const m3LargeLarge1440DpLayoutRegionsOn1Offset = 0;
    static const m3LargeLarge1440DpLayoutRegionsOn1Pattern = columns;
    static const m3LargeLarge1440DpLayoutRegionsOn1SectionSize = 72;
    static const m3LargeLarge1440DpLayoutRegionsOn2Alignment = min;
    static const m3LargeLarge1440DpLayoutRegionsOn2Count = 1;
    static const m3LargeLarge1440DpLayoutRegionsOn2GutterSize = 1;
    static const m3LargeLarge1440DpLayoutRegionsOn2Offset = 0;
    static const m3LargeLarge1440DpLayoutRegionsOn2Pattern = rows;
    static const m3LargeLarge1440DpLayoutRegionsOn2SectionSize = 56;
    static const m3MediumMedium12401439dpLayoutRegionsOffAlignment = stretch;
    static const m3MediumMedium12401439dpLayoutRegionsOffCount = 12;
    static const m3MediumMedium12401439dpLayoutRegionsOffGutterSize = 24;
    static const m3MediumMedium12401439dpLayoutRegionsOffOffset = 200;
    static const m3MediumMedium12401439dpLayoutRegionsOffPattern = columns;
    static const m3MediumMedium12401439dpLayoutRegionsOn0Alignment = stretch;
    static const m3MediumMedium12401439dpLayoutRegionsOn0Count = 12;
    static const m3MediumMedium12401439dpLayoutRegionsOn0GutterSize = 24;
    static const m3MediumMedium12401439dpLayoutRegionsOn0Offset = 200;
    static const m3MediumMedium12401439dpLayoutRegionsOn0Pattern = columns;
    static const m3MediumMedium12401439dpLayoutRegionsOn1Alignment = min;
    static const m3MediumMedium12401439dpLayoutRegionsOn1Count = 1;
    static const m3MediumMedium12401439dpLayoutRegionsOn1GutterSize = 1;
    static const m3MediumMedium12401439dpLayoutRegionsOn1Offset = 0;
    static const m3MediumMedium12401439dpLayoutRegionsOn1Pattern = columns;
    static const m3MediumMedium12401439dpLayoutRegionsOn1SectionSize = 72;
    static const m3MediumMedium12401439dpLayoutRegionsOn2Alignment = min;
    static const m3MediumMedium12401439dpLayoutRegionsOn2Count = 1;
    static const m3MediumMedium12401439dpLayoutRegionsOn2GutterSize = 1;
    static const m3MediumMedium12401439dpLayoutRegionsOn2Offset = 0;
    static const m3MediumMedium12401439dpLayoutRegionsOn2Pattern = rows;
    static const m3MediumMedium12401439dpLayoutRegionsOn2SectionSize = 56;
    static const m3MediumMedium9051239dpLayoutRegionsOffAlignment = stretch;
    static const m3MediumMedium9051239dpLayoutRegionsOffCount = 12;
    static const m3MediumMedium9051239dpLayoutRegionsOffGutterSize = 24;
    static const m3MediumMedium9051239dpLayoutRegionsOffOffset = 24;
    static const m3MediumMedium9051239dpLayoutRegionsOffPattern = columns;
    static const m3MediumMedium9051239dpLayoutRegionsOn0Alignment = max;
    static const m3MediumMedium9051239dpLayoutRegionsOn0Count = 12;
    static const m3MediumMedium9051239dpLayoutRegionsOn0GutterSize = 12;
    static const m3MediumMedium9051239dpLayoutRegionsOn0Offset = 32;
    static const m3MediumMedium9051239dpLayoutRegionsOn0Pattern = columns;
    static const m3MediumMedium9051239dpLayoutRegionsOn0SectionSize = 52;
    static const m3MediumMedium9051239dpLayoutRegionsOn1Alignment = min;
    static const m3MediumMedium9051239dpLayoutRegionsOn1Count = 1;
    static const m3MediumMedium9051239dpLayoutRegionsOn1GutterSize = 1;
    static const m3MediumMedium9051239dpLayoutRegionsOn1Offset = 0;
    static const m3MediumMedium9051239dpLayoutRegionsOn1Pattern = columns;
    static const m3MediumMedium9051239dpLayoutRegionsOn1SectionSize = 72;
    static const m3MediumMedium9051239dpLayoutRegionsOn2Alignment = min;
    static const m3MediumMedium9051239dpLayoutRegionsOn2Count = 1;
    static const m3MediumMedium9051239dpLayoutRegionsOn2GutterSize = 1;
    static const m3MediumMedium9051239dpLayoutRegionsOn2Offset = 0;
    static const m3MediumMedium9051239dpLayoutRegionsOn2Pattern = rows;
    static const m3MediumMedium9051239dpLayoutRegionsOn2SectionSize = 56;
    static const m3SmallSmall600904dpLayoutRegionsOffAlignment = stretch;
    static const m3SmallSmall600904dpLayoutRegionsOffCount = 8;
    static const m3SmallSmall600904dpLayoutRegionsOffGutterSize = 16;
    static const m3SmallSmall600904dpLayoutRegionsOffOffset = 32;
    static const m3SmallSmall600904dpLayoutRegionsOffPattern = columns;
    static const m3SmallSmall600904dpLayoutRegionsOn0Alignment = max;
    static const m3SmallSmall600904dpLayoutRegionsOn0Count = 8;
    static const m3SmallSmall600904dpLayoutRegionsOn0GutterSize = 16;
    static const m3SmallSmall600904dpLayoutRegionsOn0Offset = 32;
    static const m3SmallSmall600904dpLayoutRegionsOn0Pattern = columns;
    static const m3SmallSmall600904dpLayoutRegionsOn0SectionSize = 46;
    static const m3SmallSmall600904dpLayoutRegionsOn1Alignment = min;
    static const m3SmallSmall600904dpLayoutRegionsOn1Count = 1;
    static const m3SmallSmall600904dpLayoutRegionsOn1GutterSize = 1;
    static const m3SmallSmall600904dpLayoutRegionsOn1Offset = 0;
    static const m3SmallSmall600904dpLayoutRegionsOn1Pattern = columns;
    static const m3SmallSmall600904dpLayoutRegionsOn1SectionSize = 72;
    static const m3SmallSmall600904dpLayoutRegionsOn2Alignment = min;
    static const m3SmallSmall600904dpLayoutRegionsOn2Count = 1;
    static const m3SmallSmall600904dpLayoutRegionsOn2GutterSize = 1;
    static const m3SmallSmall600904dpLayoutRegionsOn2Offset = 0;
    static const m3SmallSmall600904dpLayoutRegionsOn2Pattern = rows;
    static const m3SmallSmall600904dpLayoutRegionsOn2SectionSize = 56;
}