
//
// style_dictionary_effect.dart
//

// Do not edit directly
// Generated on Mon, 17 Jan 2022 17:06:35 GMT



import 'dart:ui';

class StyleDictionaryEffect {
  StyleDictionaryEffect._();

    static const m3ElevationDark10Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationDark10OffsetX = 0;
    static const m3ElevationDark10OffsetY = 1;
    static const m3ElevationDark10Radius = 2;
    static const m3ElevationDark10Spread = 0;
    static const m3ElevationDark10Type = dropShadow;
    static const m3ElevationDark11Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationDark11OffsetX = 0;
    static const m3ElevationDark11OffsetY = 1;
    static const m3ElevationDark11Radius = 3;
    static const m3ElevationDark11Spread = 1;
    static const m3ElevationDark11Type = dropShadow;
    static const m3ElevationDark20Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationDark20OffsetX = 0;
    static const m3ElevationDark20OffsetY = 1;
    static const m3ElevationDark20Radius = 2;
    static const m3ElevationDark20Spread = 0;
    static const m3ElevationDark20Type = dropShadow;
    static const m3ElevationDark21Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationDark21OffsetX = 0;
    static const m3ElevationDark21OffsetY = 2;
    static const m3ElevationDark21Radius = 6;
    static const m3ElevationDark21Spread = 2;
    static const m3ElevationDark21Type = dropShadow;
    static const m3ElevationDark30Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationDark30OffsetX = 0;
    static const m3ElevationDark30OffsetY = 1;
    static const m3ElevationDark30Radius = 3;
    static const m3ElevationDark30Spread = 0;
    static const m3ElevationDark30Type = dropShadow;
    static const m3ElevationDark31Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationDark31OffsetX = 0;
    static const m3ElevationDark31OffsetY = 4;
    static const m3ElevationDark31Radius = 8;
    static const m3ElevationDark31Spread = 3;
    static const m3ElevationDark31Type = dropShadow;
    static const m3ElevationDark40Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationDark40OffsetX = 0;
    static const m3ElevationDark40OffsetY = 2;
    static const m3ElevationDark40Radius = 3;
    static const m3ElevationDark40Spread = 0;
    static const m3ElevationDark40Type = dropShadow;
    static const m3ElevationDark41Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationDark41OffsetX = 0;
    static const m3ElevationDark41OffsetY = 6;
    static const m3ElevationDark41Radius = 10;
    static const m3ElevationDark41Spread = 4;
    static const m3ElevationDark41Type = dropShadow;
    static const m3ElevationDark50Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationDark50OffsetX = 0;
    static const m3ElevationDark50OffsetY = 4;
    static const m3ElevationDark50Radius = 4;
    static const m3ElevationDark50Spread = 0;
    static const m3ElevationDark50Type = dropShadow;
    static const m3ElevationDark51Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationDark51OffsetX = 0;
    static const m3ElevationDark51OffsetY = 8;
    static const m3ElevationDark51Radius = 12;
    static const m3ElevationDark51Spread = 6;
    static const m3ElevationDark51Type = dropShadow;
    static const m3ElevationLight10Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationLight10OffsetX = 0;
    static const m3ElevationLight10OffsetY = 1;
    static const m3ElevationLight10Radius = 3;
    static const m3ElevationLight10Spread = 1;
    static const m3ElevationLight10Type = dropShadow;
    static const m3ElevationLight11Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationLight11OffsetX = 0;
    static const m3ElevationLight11OffsetY = 1;
    static const m3ElevationLight11Radius = 2;
    static const m3ElevationLight11Spread = 0;
    static const m3ElevationLight11Type = dropShadow;
    static const m3ElevationLight20Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationLight20OffsetX = 0;
    static const m3ElevationLight20OffsetY = 2;
    static const m3ElevationLight20Radius = 6;
    static const m3ElevationLight20Spread = 2;
    static const m3ElevationLight20Type = dropShadow;
    static const m3ElevationLight21Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationLight21OffsetX = 0;
    static const m3ElevationLight21OffsetY = 1;
    static const m3ElevationLight21Radius = 2;
    static const m3ElevationLight21Spread = 0;
    static const m3ElevationLight21Type = dropShadow;
    static const m3ElevationLight30Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationLight30OffsetX = 0;
    static const m3ElevationLight30OffsetY = 1;
    static const m3ElevationLight30Radius = 3;
    static const m3ElevationLight30Spread = 0;
    static const m3ElevationLight30Type = dropShadow;
    static const m3ElevationLight31Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationLight31OffsetX = 0;
    static const m3ElevationLight31OffsetY = 4;
    static const m3ElevationLight31Radius = 8;
    static const m3ElevationLight31Spread = 3;
    static const m3ElevationLight31Type = dropShadow;
    static const m3ElevationLight40Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationLight40OffsetX = 0;
    static const m3ElevationLight40OffsetY = 2;
    static const m3ElevationLight40Radius = 3;
    static const m3ElevationLight40Spread = 0;
    static const m3ElevationLight40Type = dropShadow;
    static const m3ElevationLight41Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationLight41OffsetX = 0;
    static const m3ElevationLight41OffsetY = 6;
    static const m3ElevationLight41Radius = 10;
    static const m3ElevationLight41Spread = 4;
    static const m3ElevationLight41Type = dropShadow;
    static const m3ElevationLight50Color = rgba(0, 0, 0, 0.3);
    static const m3ElevationLight50OffsetX = 0;
    static const m3ElevationLight50OffsetY = 4;
    static const m3ElevationLight50Radius = 4;
    static const m3ElevationLight50Spread = 0;
    static const m3ElevationLight50Type = dropShadow;
    static const m3ElevationLight51Color = rgba(0, 0, 0, 0.15);
    static const m3ElevationLight51OffsetX = 0;
    static const m3ElevationLight51OffsetY = 8;
    static const m3ElevationLight51Radius = 12;
    static const m3ElevationLight51Spread = 6;
    static const m3ElevationLight51Type = dropShadow;
}